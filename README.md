# Makovskysocnetwork   

** Functionality: **  
+ registration  
+ edit profile  
+ display profile  
+ upload avatar  
+ authentication with Spring Security  
+ ajax search autocomplete  
+ ajax search with pagination  
+ users export to xml  
+ messaging through WebSocket  
+ posting on wall  
+ groups creation  
+ adding/removing friends  
+ adding/removing group followers   

** Tools: **  
JDK 7, 8, Spring 5, JPA 2 / Hibernate 5, XStream, jQuery 3, JUnit 4, Mockito, Maven 3, Git / Bitbucket, Tomcat 7, MySQL, IntelliJIDEA 2018.  

** Notes: **  
SQL ddl is located in the `dao/src/main/resources/social-network-db.sql`  

** Screenshots: **  
![Image](https://i.gyazo.com/41cfecf80993662a5a76538cbd4ec9ed.png)  
![Image](https://i.gyazo.com/48ab355546fb51a3e518ffe8d2b312ca.png)  
![Image](https://i.gyazo.com/c2c29ac3a64749241e93d4cf7741ba03.png)  
![Image](https://i.gyazo.com/f5a92f4758330f762b6ce91accecd026.png)  
![Image](https://i.gyazo.com/2bcbf7472f471a61d8d62e887ba47bdc.png)  


The project hosted at https://makovskysocnetwork.herokuapp.com/  

user: user1  
password: user11  
  
_  
**Makovsky Maxim**  
Training getJavaJob  
http://www.getjavajob.com  