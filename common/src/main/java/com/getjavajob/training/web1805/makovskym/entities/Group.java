package com.getjavajob.training.web1805.makovskym.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.WhereJoinTable;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "groups_tb")
public class Group implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "group_id")
    private Long id;
    @Column(name = "name", nullable = false)
    private String nameOfGroup;
    @Column(name = "description")
    private String groupDescription;
    @Column(name = "created_date", nullable = false)
    private Date createdDate;
    @ManyToOne
    @JoinColumn(name = "creator_id")
    @JsonIgnore
    private Account creator;
    @ManyToOne
    @JoinColumn(name = "photo_id")
    private Photo mainPhoto;
    @ManyToMany(targetEntity = Account.class, fetch = FetchType.LAZY)
    @JoinTable(name = "followers_tb",
            joinColumns = {@JoinColumn(name = "group_id")},
            inverseJoinColumns = {@JoinColumn(name = "follower_id")})
    @WhereJoinTable(clause = "ROLE = 'FOLLOWER' ")
    @JsonIgnore
    private List<Account> followers;
    @ManyToMany(targetEntity = Account.class, fetch = FetchType.LAZY)
    @JoinTable(name = "followers_tb",
            joinColumns = {@JoinColumn(name = "group_id")},
            inverseJoinColumns = {@JoinColumn(name = "follower_id")})
    @WhereJoinTable(clause = "ROLE = 'ADMIN' ")
    @JsonIgnore
    private List<Account> admins;
    @ManyToMany(targetEntity = Account.class, fetch = FetchType.LAZY)
    @JoinTable(name = "followers_tb",
            joinColumns = {@JoinColumn(name = "group_id")},
            inverseJoinColumns = {@JoinColumn(name = "follower_id")})
    @WhereJoinTable(clause = "ROLE = 'MODERATOR' ")
    @JsonIgnore
    private List<Account> moderators;
    @ManyToMany(targetEntity = Account.class, fetch = FetchType.LAZY)
    @JoinTable(name = "followers_tb",
            joinColumns = {@JoinColumn(name = "group_id")},
            inverseJoinColumns = {@JoinColumn(name = "follower_id")})
    @WhereJoinTable(clause = "ROLE = 'REQUEST' ")
    @JsonIgnore
    private List<Account> requests;

    public Group() {
    }

    public Group(String name, String description, Date createdDate, Account creator) {
        this.nameOfGroup = name;
        this.groupDescription = description;
        this.createdDate = createdDate;
        this.creator = creator;
        followers = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameOfGroup() {
        return nameOfGroup;
    }

    public void setNameOfGroup(String nameOfGroup) {
        this.nameOfGroup = nameOfGroup;
    }

    public String getGroupDescription() {
        return groupDescription;
    }

    public void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription;
    }

    public Account getCreator() {
        return creator;
    }

    public void setCreator(Account creator) {
        this.creator = creator;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Photo getMainPhoto() {
        return mainPhoto;
    }

    public void setMainPhoto(Photo mainPhoto) {
        this.mainPhoto = mainPhoto;
    }

    public List<Account> getFollowers() {
        return followers;
    }

    public void setFollowers(List<Account> followers) {
        this.followers = followers;
    }

    public List<Account> getAdmins() {
        return admins;
    }

    public void setAdmins(List<Account> admins) {
        this.admins = admins;
    }

    public List<Account> getModerators() {
        return moderators;
    }

    public void setModerators(List<Account> moderators) {
        this.moderators = moderators;
    }

    public List<Account> getRequests() {
        return requests;
    }

    public void setRequests(List<Account> requests) {
        this.requests = requests;
    }

    @Override
    public String toString() {
        return "Group [name: " + nameOfGroup + "\nDescription: " + groupDescription + "]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Group group = (Group) o;
        return Objects.equals(id, group.id) &&
                Objects.equals(nameOfGroup, group.nameOfGroup) &&
                Objects.equals(groupDescription, group.groupDescription) &&
                Objects.equals(createdDate, group.createdDate) &&
                Objects.equals(creator, group.creator) &&
                Objects.equals(mainPhoto, group.mainPhoto) &&
                Objects.equals(followers, group.followers) &&
                Objects.equals(admins, group.admins) &&
                Objects.equals(moderators, group.moderators) &&
                Objects.equals(requests, group.requests);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nameOfGroup, groupDescription, createdDate, creator, mainPhoto, followers, admins, moderators, requests);
    }
}
