package com.getjavajob.training.web1805.makovskym.keys;

import java.io.Serializable;

public class MembershipsKey implements Serializable {
    private Long groupId;
    private Long accountId;

    public MembershipsKey() {
    }

    public MembershipsKey(Long groupId, Long accountId) {
        this.groupId = groupId;
        this.accountId = accountId;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MembershipsKey that = (MembershipsKey) o;
        if (groupId != null ? !groupId.equals(that.groupId) : that.groupId != null) return false;
        return accountId != null ? accountId.equals(that.accountId) : that.accountId == null;
    }

    @Override
    public int hashCode() {
        int result = groupId != null ? groupId.hashCode() : 0;
        result = 31 * result + (accountId != null ? accountId.hashCode() : 0);
        return result;
    }
}
