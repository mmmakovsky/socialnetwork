package com.getjavajob.training.web1805.makovskym.keys;

import java.io.Serializable;

public class FriendshipsKey implements Serializable {
    private Long accountId;
    private Long friendId;

    public FriendshipsKey() {
    }

    public FriendshipsKey(Long accountId, Long friendId) {
        this.accountId = accountId;
        this.friendId = friendId;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Long getFriendId() {
        return friendId;
    }

    public void setFriendId(Long friendId) {
        this.friendId = friendId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FriendshipsKey that = (FriendshipsKey) o;
        if (accountId != null ? !accountId.equals(that.accountId) : that.accountId != null) return false;
        return friendId != null ? friendId.equals(that.friendId) : that.friendId == null;
    }

    @Override
    public int hashCode() {
        int result = accountId != null ? accountId.hashCode() : 0;
        result = 31 * result + (friendId != null ? friendId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "FriendshipsKey{" +
                "accountId=" + accountId +
                ", friendId=" + friendId +
                '}';
    }
}
