package com.getjavajob.training.web1805.makovskym.entities;

import com.getjavajob.training.web1805.makovskym.enums.RoleInGroup;
import com.getjavajob.training.web1805.makovskym.keys.MembershipsKey;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "followers_tb")
@IdClass(MembershipsKey.class)
public class Memberships implements Serializable {
    @Id
    @Column(name = "group_id", nullable = false)
    private Long groupId;
    @Id
    @Column(name = "follower_id", nullable = false)
    private Long accountId;
    @Column(name = "role", nullable = false)
    @NotNull
    @Enumerated(EnumType.STRING)
    private RoleInGroup role;

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public RoleInGroup getRole() {
        return role;
    }

    public void setRole(RoleInGroup role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Memberships that = (Memberships) o;

        if (groupId != null ? !groupId.equals(that.groupId) : that.groupId != null) return false;
        if (accountId != null ? !accountId.equals(that.accountId) : that.accountId != null) return false;
        return role != null ? role.equals(that.role) : that.role == null;
    }

    @Override
    public int hashCode() {
        int result = groupId != null ? groupId.hashCode() : 0;
        result = 31 * result + (accountId != null ? accountId.hashCode() : 0);
        result = 31 * result + (role != null ? role.hashCode() : 0);
        return result;
    }
}
