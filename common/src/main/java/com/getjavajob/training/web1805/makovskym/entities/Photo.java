package com.getjavajob.training.web1805.makovskym.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.Arrays;

@Entity

@XmlAccessorType(XmlAccessType.FIELD)
@Table(name = "photos_tb")
public class Photo implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "photo_id")
    private Long id;
    @Column(name = "photo", nullable = false)
    @XmlTransient
    private byte[] image;
    @ManyToOne
    @JoinColumn(name = "author_id", nullable = false)
    @JsonIgnore
    @XmlTransient
    private Account author;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] photo) {
        this.image = photo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Photo photo1 = (Photo) o;
        if (id != null ? !id.equals(photo1.id) : photo1.id != null) return false;
        if (!Arrays.equals(image, photo1.image)) return false;
        return author != null ? author.equals(photo1.author) : photo1.author == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + Arrays.hashCode(image);
        result = 31 * result + (author != null ? author.hashCode() : 0);
        return result;
    }

    public Account getAuthor() {
        return author;
    }

    public void setAuthor(Account author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "Photo{" +
                "id=" + id +
                ", author=" + author +
                '}';
    }
}
