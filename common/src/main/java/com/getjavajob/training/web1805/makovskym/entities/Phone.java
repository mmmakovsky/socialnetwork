package com.getjavajob.training.web1805.makovskym.entities;

import com.getjavajob.training.web1805.makovskym.enums.PhoneType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;

@Entity
@XmlAccessorType(XmlAccessType.FIELD)
@Table(name = "phones_tb")
public class Phone implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "phone", nullable = false)
    private String phoneNumber;
    @ManyToOne
    @JoinColumn(name = "account_id", nullable = false)
    @XmlTransient
    private Account account;
    @Column(name = "type", nullable = false)
    @NotNull
    @Enumerated(EnumType.STRING)
    private PhoneType type;

    public Phone() {
    }

    public Phone(String phoneNumber, Account account, PhoneType type) {
        this.phoneNumber = phoneNumber;
        this.account = account;
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public PhoneType getType() {
        return type;
    }

    public void setType(PhoneType type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Phone phone1 = (Phone) o;

        if (phoneNumber != null ? !phoneNumber.equals(phone1.phoneNumber) : phone1.phoneNumber != null) return false;
        if (account != null ? !account.equals(phone1.account) : phone1.account != null) return false;
        return type != null ? type.equals(phone1.type) : phone1.type == null;
    }

    @Override
    public int hashCode() {
        int result = phoneNumber != null ? phoneNumber.hashCode() : 0;
        result = 31 * result + (account != null ? account.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }
}
