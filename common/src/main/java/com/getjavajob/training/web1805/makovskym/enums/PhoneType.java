package com.getjavajob.training.web1805.makovskym.enums;

public enum PhoneType {
    HOME, WORK;

    PhoneType() {
    }
}
