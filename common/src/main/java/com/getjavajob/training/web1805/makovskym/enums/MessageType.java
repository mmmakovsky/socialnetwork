package com.getjavajob.training.web1805.makovskym.enums;

public enum MessageType {
    PRIVATE, GROUP, PUBLIC;

    MessageType() {
    }
}
