package com.getjavajob.training.web1805.makovskym.entities;

import com.getjavajob.training.web1805.makovskym.enums.MessageType;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "messages_tb")
public class Message implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "msg_id")
    private Long id;
    @ManyToOne
    @JoinColumn(name = "author_id", nullable = false)
    private Account author;
    @Column(name = "sent_date", nullable = false)
    private Timestamp date;
    @Column(name = "msg")
    private String msg;
    @Column(name = "photo_id")
    private Long photoID;
    @Column(name = "destination_id", nullable = false)
    private Long destinationId;
    @Column(name = "type", nullable = false)
    @Enumerated(EnumType.STRING)
    private MessageType type;

    public Message() {
    }

    public Message(Account author, Timestamp date, String msg, long photoID, long destinationId) {
        this.author = author;
        this.date = date;
        this.msg = msg;
        this.photoID = photoID;
        this.destinationId = destinationId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        if (id != null ? !id.equals(message.id) : message.id != null) return false;
        if (author != null ? !author.equals(message.author) : message.author != null) return false;
        if (date != null ? !date.equals(message.date) : message.date != null) return false;
        if (msg != null ? !msg.equals(message.msg) : message.msg != null) return false;
        if (destinationId != null ? !destinationId.equals(message.destinationId) : message.destinationId != null)
            return false;
        return type != null ? type.equals(message.type) : message.type == null;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, author, date, msg, photoID, destinationId);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Account getAuthor() {
        return author;
    }

    public void setAuthor(Account author) {
        this.author = author;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Long getPhotoID() {
        return photoID;
    }

    public void setPhotoID(Long photoID) {
        this.photoID = photoID;
    }

    public Long getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(Long destinationId) {
        this.destinationId = destinationId;
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return author.getSecondName() + ' ' + author.getFirstName() + '\n' + date + '\n' + msg;
    }
}
