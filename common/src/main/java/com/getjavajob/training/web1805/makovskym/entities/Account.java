package com.getjavajob.training.web1805.makovskym.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.getjavajob.training.web1805.makovskym.enums.AccountRoles;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Where;
import org.hibernate.annotations.WhereJoinTable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.sql.Date;
import java.util.*;

@Entity
@XmlRootElement(name = "account")
@XmlAccessorType(XmlAccessType.FIELD)
@Table(name = "Accounts_tb")
public class Account implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "account_id")
    @XmlTransient
    private Long id;
    @Column(name = "second_name", nullable = false)
    private String secondName;
    @Column(name = "first_name", nullable = false)
    private String firstName;
    @Column(name = "middle_name")
    private String middleName;
    @XmlTransient
    private Date birthday;
    @Column(name = "home_address")
    private String homeAddress;
    @Column(name = "work_address")
    private String workAddress;
    private String email;
    private Integer icq;
    private String skype;
    @Column(name = "more_info")
    private String moreInformation;
    @Column(name = "registration_date", nullable = false)
    @XmlTransient
    private Date registrationDate;
    @XmlTransient
    @Column(name = "login", nullable = false)
    private String login;
    @XmlTransient
    @Column(nullable = false)
    private String password;
    @Column(name = "role", nullable = false)
    @Enumerated(EnumType.STRING)
    private AccountRoles accountRole;
    @OneToMany(mappedBy = "account")
    @Where(clause = "type = 'HOME' ")
    @JsonIgnore
    private List<Phone> homePhones;
    @OneToMany(mappedBy = "account")
    @Where(clause = "type = 'WORK' ")
    @JsonIgnore
    private List<Phone> workPhones;
    @ManyToMany(targetEntity = Account.class)
    @JoinTable(name = "friendships_tb",
            joinColumns = {@JoinColumn(name = "account_id")},
            inverseJoinColumns = {@JoinColumn(name = "friend_id")})
    @WhereJoinTable(clause = "conditions = 'FRIEND' ")
    @JsonIgnore
    @Fetch(value = FetchMode.SUBSELECT)
    @XmlTransient
    private List<Account> friends;
    @ManyToMany(targetEntity = Account.class)
    @JoinTable(name = "friendships_tb",
            joinColumns = {@JoinColumn(name = "account_id")},
            inverseJoinColumns = {@JoinColumn(name = "friend_id")})
    @WhereJoinTable(clause = "conditions = 'FOLLOWER' ")
    @JsonIgnore
    @XmlTransient
    private List<Account> followers;
    @ManyToMany(targetEntity = Account.class)
    @JoinTable(name = "friendships_tb",
            joinColumns = {@JoinColumn(name = "friend_id")},
            inverseJoinColumns = {@JoinColumn(name = "account_id")})
    @WhereJoinTable(clause = "conditions = 'FOLLOWER' ")
    @JsonIgnore
    @XmlTransient
    private List<Account> subscriptions;
    @ManyToMany(targetEntity = Group.class)
    @JoinTable(name = "followers_tb",
            joinColumns = {@JoinColumn(name = "follower_id")},
            inverseJoinColumns = {@JoinColumn(name = "group_id")})
    @WhereJoinTable(clause = "role = 'follower' or role='ADMIN'")
    @JsonIgnore
    @XmlTransient
    private List<Group> groups;
    @ManyToMany(targetEntity = Group.class)
    @JoinTable(name = "followers_tb",
            joinColumns = {@JoinColumn(name = "follower_id")},
            inverseJoinColumns = {@JoinColumn(name = "group_id")})
    @WhereJoinTable(clause = "role = 'REQUEST'")
    @JsonIgnore
    @XmlTransient
    private List<Group> subscribedGroups;
    @ManyToOne
    @JoinColumn(name = "photo_id")
    private Photo mainPhoto;

    public Account() {
    }

    public Account(String secondName, String firstName,
                   String login, String password, AccountRoles accountRole, Date registrationDate) {
        this.secondName = secondName;
        this.firstName = firstName;
        this.login = login;
        this.password = password;
        this.accountRole = accountRole;
        this.registrationDate = registrationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Account account = (Account) o;
        return id.equals(account.id) &&
                Objects.equals(secondName, account.secondName) &&
                Objects.equals(firstName, account.firstName) &&
                Objects.equals(registrationDate, account.registrationDate) &&
                Objects.equals(login, account.login);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, secondName, firstName, middleName, birthday, homeAddress, workAddress, email, icq, skype, registrationDate, login, password);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Photo getMainPhoto() {
        return mainPhoto;
    }

    public void setMainPhoto(Photo mainPhoto) {
        this.mainPhoto = mainPhoto;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getIcq() {
        return icq;
    }

    public void setIcq(Integer icq) {
        this.icq = icq;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getMoreInformation() {
        return moreInformation;
    }

    public void setMoreInformation(String moreInformation) {
        this.moreInformation = moreInformation;
    }

    public List<Account> getFriends() {
        return friends;
    }

    public void setFriends(List<Account> friends) {
        this.friends = friends;
    }

    public List<Account> getFollowers() {
        return followers;
    }

    public void setFollowers(List<Account> followers) {
        this.followers = followers;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public List<Group> getSubscribedGroups() {
        return subscribedGroups;
    }

    public void setSubscribedGroups(List<Group> subscribedGroups) {
        this.subscribedGroups = subscribedGroups;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Set<GrantedAuthority> roles = new HashSet<>();
        roles.add(new SimpleGrantedAuthority("ROLE_" + accountRole));
        return roles;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public String getUsername() {
        return getLogin();
    }

    public void setUsername(String login) {
        setLogin(login);
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public List<Phone> getHomePhones() {
        return homePhones;
    }

    public void setHomePhones(List<Phone> homePhones) {
        this.homePhones = homePhones;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public AccountRoles getAccountRole() {
        return accountRole;
    }

    public void setAccountRole(AccountRoles accountRole) {
        this.accountRole = accountRole;
    }

    public List<Phone> getWorkPhones() {
        return workPhones;
    }

    public void setWorkPhones(List<Phone> workPhones) {
        this.workPhones = workPhones;
    }

    public List<Account> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(List<Account> subscriptions) {
        this.subscriptions = subscriptions;
    }

    @Override
    public String toString() {
        return "Account [firstName= " + firstName + ", secondName = " + secondName + ", middle name = " + middleName + "\n"
                + "Birthday: " + birthday;
    }
}

