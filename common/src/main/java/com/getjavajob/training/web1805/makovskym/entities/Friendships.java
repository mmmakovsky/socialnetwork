package com.getjavajob.training.web1805.makovskym.entities;

import com.getjavajob.training.web1805.makovskym.enums.FriendsRelationships;
import com.getjavajob.training.web1805.makovskym.keys.FriendshipsKey;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "friendships_tb")
@IdClass(FriendshipsKey.class)
public class Friendships implements Serializable {
    @Id
    @Column(name = "account_id", nullable = false)
    private Long accountId;
    @Id
    @Column(name = "friend_id", nullable = false)
    private Long friendId;
    @Column(name = "conditions", nullable = false)
    @NotNull
    @Enumerated(EnumType.STRING)
    private FriendsRelationships relationship;

    public Friendships(Long accountId, Long friendId, FriendsRelationships relationship) {
        this.accountId = accountId;
        this.friendId = friendId;
        this.relationship = relationship;
    }

    public Friendships() {
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Long getFriendId() {
        return friendId;
    }

    public void setFriendId(Long friendId) {
        this.friendId = friendId;
    }

    public FriendsRelationships getRelationship() {
        return relationship;
    }

    public void setRelationship(FriendsRelationships relationship) {
        this.relationship = relationship;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Friendships that = (Friendships) o;
        if (!accountId.equals(that.accountId)) {
            return false;
        }
        if (!friendId.equals(that.friendId)) {
            return false;
        }
        return relationship.equals(that.relationship);
    }

    @Override
    public int hashCode() {
        int result = accountId.hashCode();
        result = 31 * result + friendId.hashCode();
        result = 31 * result + relationship.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Friendships{" +
                "accountId=" + accountId +
                ", friendId=" + friendId +
                ", relationship='" + relationship + '\'' +
                '}';
    }
}
