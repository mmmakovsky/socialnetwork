package com.getjavajob.training.web1805.makovskym.enums;

public enum AccountRoles {
    ADMIN, MODERATOR, USER;

    AccountRoles() {
    }
}
