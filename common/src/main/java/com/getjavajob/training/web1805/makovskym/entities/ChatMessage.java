package com.getjavajob.training.web1805.makovskym.entities;

import java.sql.Timestamp;

public class ChatMessage {
    private String content;
    private String sender;
    private Timestamp dtime;
    private Long dest;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public Timestamp getDtime() {
        return dtime;
    }

    public void setDtime(Timestamp dtime) {
        this.dtime = dtime;
    }

    public Long getDest() {
        return dest;
    }

    public void setDest(Long dest) {
        this.dest = dest;
    }
}
