package com.getjavajob.training.web1805.makovskym.enums;

public enum FriendsRelationships {
    FRIEND, FOLLOWER;

    FriendsRelationships() {
    }
}
