package com.getjavajob.training.web1805.makovskym.enums;

public enum RoleInGroup {
    ADMIN, FOLLOWER, REQUEST, MODERATOR;

    RoleInGroup() {
    }
}
