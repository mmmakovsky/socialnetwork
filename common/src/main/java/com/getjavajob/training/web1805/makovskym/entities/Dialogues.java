package com.getjavajob.training.web1805.makovskym.entities;

import javax.persistence.*;

@Entity
@Table(name = "dialogues_tb")
public class Dialogues {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "dialogue_id")
    private Long id;
    @Column(name = "account_id", nullable = false)
    private Long account;
    @Column(name = "friend_id", nullable = false)
    private Long friend;

    public Dialogues() {
    }

    public Dialogues(Long account, Long friend) {
        this.account = account;
        this.friend = friend;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAccount() {
        return account;
    }

    public void setAccount(Long account) {
        this.account = account;
    }

    public Long getFriend() {
        return friend;
    }

    public void setFriend(Long friend) {
        this.friend = friend;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dialogues dialogues = (Dialogues) o;
        if (id != null ? !id.equals(dialogues.id) : dialogues.id != null) {
            return false;
        }
        if (account != null ? !account.equals(dialogues.account) : dialogues.account != null) {
            return false;
        }
        return friend != null ? friend.equals(dialogues.friend) : dialogues.friend == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (account != null ? account.hashCode() : 0);
        result = 31 * result + (friend != null ? friend.hashCode() : 0);
        return result;
    }
}
