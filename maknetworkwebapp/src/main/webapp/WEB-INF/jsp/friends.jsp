<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="<c:url value="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/phones.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <jsp:include page="/WEB-INF/jsp/upperPanel.jsp"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/m.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery.js"></script>
</head>
<body style="background-color:rgb(229, 224, 255);">
<hr>
<div id="tabs" class="w3-container w3-content mycont"
     style="width:500px; border-radius:5px;  border: 1px solid #777777;">
    <ul>
        <li><a href="#tabs-1">Друзья</a></li>
        <li><a href="#tabs-2">Подписчики</a></li>
        <li><a href="#tabs-3">Подписки</a></li>
    </ul>
    <div id="tabs-1">
        <table class="w3-table">
            <div class="w3-card">
                <tbody>
                <%--@elvariable id="friends" type="java.util.List"--%>
                <c:forEach items="${friends}" var="follower">
                    <tr>
                        <td>
                            <div class="w3-row-padding w3-margin-top">
                                <div>
                                    <div class="w3-quarter" style="width:70px">
                                        <div>
                                            <image src="/image/${follower.mainPhoto.id}"
                                                   style="width:45px;height:50px;"></image>
                                        </div>
                                    </div>
                                    <div class="w3-half">
                                        <a onclick="window.location='/account?id=${follower.id}';">
                                                ${follower.firstName} ${follower.secondName}
                                        </a>
                                        <a onclick="window.location='/account/messages?id=${follower.id}';"
                                           method="post">
                                            <p style="font-size:60%;">Написать сообщение</p>
                                        </a>
                                    </div>
                                    <div class="w3-quarter">
                                        <form name="deleteFriends" action="/friend/delete/${follower.id}" method="post">
                                            <input class="ui-button ui-widget ui-corner-all  w3-hover-blue "
                                                   type="submit" value="Удалить"/>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </div>
        </table>
    </div>
    <div id="tabs-2">
        <table class="w3-table w3-card">
            <div class="w3-card">
                <tbody>
                <%--@elvariable id="followers" type="java.util.List"--%>
                <c:forEach items="${followers}" var="friend">
                    <tr>
                        <td>
                            <div class="w3-row-padding w3-margin-top">
                                <div>
                                    <div class="w3-quarter" style="width:70px">
                                        <div>
                                            <image src="/image/${friend.mainPhoto.id}"
                                                   style="width:45px;height:50px;"></image>
                                        </div>
                                    </div>
                                    <div class="w3-half">
                                        <a onclick="window.location='/account?id=${friend.id}';">
                                                ${friend.firstName} ${friend.secondName}
                                        </a>
                                        <a onclick="window.location='/account/messages?id=${friend.id}';" method="post">
                                            <p style="font-size:60%;">Сообщение</p>
                                        </a>
                                    </div>
                                    <div class="w3-quarter">
                                        <form class="w3-right" action="/friend/add/${friend.id}" method="post">
                                            <input class="ui-button ui-widget ui-corner-all  w3-hover-blue "
                                                   type="submit" value="Добавить"/>
                                        </form>

                                        <form class="w3-right" action="/follower/delete/${friend.id}" method="post">
                                            <input class="ui-button ui-widget ui-corner-all  w3-hover-blue "
                                                   type="submit" value="Удалить"/>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </div>
        </table>
    </div>
    <div id="tabs-3">
        <table class="w3-table w3-card">
            <div class="w3-card">
                <tbody>
                <%--@elvariable id="subscribes" type="java.util.List"--%>
                <c:forEach items="${subscribes}" var="subscribe">
                    <tr>
                        <td>
                            <div class="w3-row-padding w3-margin-top">
                                <div>
                                    <div class="w3-quarter" style="width:70px">
                                        <div>
                                            <image src="/image?id=${subscribe.mainPhoto.id}"
                                                   style="width:45px;height:50px;"></image>
                                        </div>
                                    </div>
                                    <div class="w3-half">
                                        <a onclick="window.location='/account?id=${subscribe.id}';">
                                                ${subscribe.firstName} ${subscribe.secondName}
                                        </a>
                                        <a onclick="window.location='/account/messages?id=${subscribe.id}';"
                                           method="post">
                                            <p style="font-size:60%;">Сообщение</p>
                                        </a>
                                    </div>
                                    <div class="w3-quarter">
                                        <form class="w3-right" action="/subscribe/delete/${subscribe.id}"
                                              method="post">
                                            <input class="ui-button ui-widget ui-corner-all w3-hover-blue "
                                                   type="submit"
                                                   value="Отписаться"/>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </div>
        </table>
    </div>
</div>
</body>
</html>