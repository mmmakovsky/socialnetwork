<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Редактирование инфомации об аккаунте</title>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="<c:url value="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <link href="https://dimox.github.io/jQueryFormStyler/dist/jquery.formstyler.css" rel="stylesheet"/>
    <link href="https://dimox.github.io/jQueryFormStyler/dist/jquery.formstyler.theme.css" rel="stylesheet"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/phones.js"></script>
    <script src="https://dimox.github.io/jQueryFormStyler/dist/jquery.formstyler.js"></script>
    <script src="https://dimox.github.io/jQueryFormStyler/demo/demo.js"></script>
    <jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/upperPanel.jsp"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/m.css">
</head>
<body id="tour2" style="background-color:rgb(229, 224, 255);">
<div class="w3-row-padding w3-margin-top w3-container w3-content" id="tour1" style="max-width:1000px">
    <div class="w3-half w3-centered w3-bordered">
        <div class="w3-container w3-content w3-white w3-card mycont" style="max-width:500px; ">
            <form name="edit" id="edit" action="<c:url value="/edit"/>" method="post">
                <h2>Редактирование акккаунта </h2>
                <input type="hidden" name="id" id="id" value="${account.id}" size="40" placeholder="First name">
                <p><b>Имя:</b><br>
                    <input class="styler" type="text" name="firstName" id="firstName" value="${account.firstName}"
                           size="43"></p>
                <p><b>Фамилия:</b><br>
                    <input class="styler" type="text" name="secondName" id="secondName" value="${account.secondName}"
                           size="43"></p>
                <p><b>Отчество:</b><br>
                    <input class="styler" type="text" name="middleName" id="middleName" value="${account.middleName}"
                           size="43"></p>
                <p><b>День рождения:</b><br>
                    <input class="styler" type="date" name="birth" id="birth" value="${account.birthday}" size="43"></p>
                <p><b>Домашний адрес:</b><br>
                    <input class="styler" type="text" name="homeAddress" id="homeAddress" value="${account.homeAddress}"
                           size="43"
                    ></p>
                <p><b>Рабочий адрес:</b><br>
                    <input class="styler" type="text" name="workAddress" id="workAddress" value="${account.workAddress}"
                           size="43"
                    ></p>
                <p><b>Телефоны:</b></p>
                <p style="color:red;" id="message"></p>
                <TABLE id='phones1' style="width:40px;">
                    <TBODY>
                    <TR>
                        <TD class="w3-left"><INPUT class="styler" id='input0' type="text" size="21"/></TD>
                        <TD>
                            <div class="jq-selectbox jqselect changed" style="z-index: 10;">
                                <select id='input1'>
                                    <option>HOME</option>
                                    <option>WORK</option>
                                </select>
                            </div>
                        </TD>
                        <TD><INPUT style="width:45px;" class="styler" type='button' id='input2' value='+' onClick="
        ValidPhone();" onclick="addRow('phones')"/>
                        </TD>
                    </TR>
                    </TBODY>
                </TABLE>
                <TABLE id='phones' style="width:30px;">
                    <TBODY>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <c:forEach items="${account.homePhones}" var="phone">
                        <tr>
                            <td><input type='hidden' name='ph' value='${phone.phone}'>
                                <p style='width:180px;'>${phone.phone}
                                </p></TD>
                            <td><input type='hidden' name='ph' value='${phone.type}'>
                                <p style='width:100px;'>${phone.type}
                                </p></TD>
                            <td><INPUT style='width:45px;' class='styler' type='button' value='X'
                                       onclick='removeRow(this)'/>
                            </TD>
                        </tr>
                    </c:forEach>
                    <c:forEach items="${account.workPhones}" var="phone">
                        <tr>
                            <td><input type='hidden' name='ph' value='${phone.phone}'>
                                <p style='width:180px;'>${phone.phone}
                                </p></TD>
                            <td><input type='hidden' name='ph' value='${phone.type}'>
                                <p style='width:100px;'>${phone.type}
                                </p></TD>
                            <td><INPUT style='width:45px;' class='styler' type='button' value='X'
                                       onclick='removeRow(this)'/>
                            </TD>
                        </tr>
                    </c:forEach>
                    </TBODY>
                </TABLE>
                <p><b>Email:</b><br>
                    <input class="styler" type="text" name="email" id="email" value="${account.email}" size="43"
                           placeholder="email"></p>
                <p><b>ICQ:</b><br>
                    <input class="styler" type="text" name="icq" id="icq" value="${account.icq}" size="43"
                           placeholder="icq"></p>
                <p><b>Skype:</b><br>
                    <input class="styler" type="text" name="skype" id="skype" value="${account.skype}" size="43"></p>
                <p><b>Дополнительная информация:</b><br>
                    <textarea class="styler" name="moreInformation" id="moreInformation" rows="10"
                              cols="45">${account.moreInformation}</textarea></p>
                <p><input class="ui-button ui-widget ui-corner-all" type="submit" value="Сохранить"
                          onClick="return confirmChanges();"/>
            </form>
        </div>
    </div>
    <div class="w3-half w3-centered w3-bordered">
        <div class="w3-container w3-content w3-white w3-card mycont" style="max-width:400px;">
            <form name="registration" action="<c:url value="/account/edit/password"/>" method="post">
                <h2>Сменить пароль</h2>
                <input class="styler" type="password" name="Old Password" id="password" size="20"
                       placeholder="Старый пароль">${IncorrectPassword}
                <p><input class="styler" type="password" name="New Password" id="newPassword" size="20"
                          placeholder="Новый пароль"></p>${NotMatchPassword}
                <p><input class="styler" type="password" name="Repeat new password" id="repeatPassword" size="20"
                          placeholder="Повторите новый пароль"></p>${NotMatchPassword}
                <p><input class="ui-button ui-widget ui-corner-all" type="submit" value="Сохранить"/>${successfully}
            </form>
        </div>

        <hr>
        <div class="w3-container w3-down w3-content w3-white w3-card mycont" style="max-width:400px;">
            <h2>Загрузить данные из XML</h2>
            <form action="${pageContext.request.contextPath}/xml/upload" method="post" enctype="multipart/form-data">
                <input class="ui-button ui-widget ui-corner-all w3-hover-blue" type="button" name="click"
                       onclick="document.getElementById('load').click()" value="Обновить фото"
                       style="width: 230px">
                <input style="display:none" id="load" type="file" name="file"
                       onchange="document.getElementById('ww').click()" value="Select File"/>
                <input style="display:none" name="ww" id="ww" type="submit" value="Upload File"/>
            </form>
        </div>
        <hr>
        <div class="w3-container w3-down w3-content w3-white w3-card mycont" style="max-width:400px;">
            <h2>Выгрузить в XML</h2>
            <p><input class="ui-button ui-widget ui-corner-all" type="button" value="Загрузить данные"
                      onClick="downloadFile();"/>
        </div>
        <hr>
        <div class="w3-container w3-down w3-content w3-white w3-card mycont" style="max-width:400px;">
            <form name="delete" action="${pageContext.request.contextPath}/account/delete" method="post">
                <h2>Удалить аккаунт</h2>
                <p><input class="ui-button ui-widget ui-corner-all" type="submit" value="Удалить аккаунт"/>
            </form>
        </div>
    </div>
</div>
</body>
</html>