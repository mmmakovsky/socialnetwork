<%@page contentType="text/html;charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html lang="en">
<head>
    <jsp:include page="/WEB-INF/jsp/upperPanel.jsp"/>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/phones.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="<c:url value="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <link href="https://dimox.github.io/jQueryFormStyler/dist/jquery.formstyler.css" rel="stylesheet"/>
    <link href="https://dimox.github.io/jQueryFormStyler/dist/jquery.formstyler.theme.css" rel="stylesheet"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/phones.js"></script>
    <script src="https://dimox.github.io/jQueryFormStyler/dist/jquery.formstyler.js"></script>
    <script src="https://dimox.github.io/jQueryFormStyler/demo/demo.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/m.css">
</head>
<body style="background-color:rgb(229, 224, 255);">
<hr>
<div class="w3-container w3-content w3-white w3-card mycont" style="max-width:500px;">
    <h2>${title}</h2>
    <c:if test="${not empty param.error}">
        <span style="font-size: x-small; color: red; "><b>Неправильный логин или пароль</b></span>
    </c:if>
    <hr>
    <form name="creating group" action="${path}" method="post">
        <input class="styler" type="text" name="id" id="id" value="${group.id}" size="50" style="display: none"
               placeholder="Group name">
        <p><b>Название группы</b><br>
            <input class="styler" type="text" name="nameOfGroup" id="nameOfGroup" value="${group.nameOfGroup}" size="50"
                   placeholder="Название">
            <br><b>Описание</b><br>
            <textarea class="styler" name="groupDescription" id="groupDescription" rows="10"
                      cols="52">${group.groupDescription}</textarea>
            <input class="ui-button ui-widget ui-corner-all w3-hover-blue" type="submit" value="Сохранить"/>
    </form>
</div>
</body>
</html>