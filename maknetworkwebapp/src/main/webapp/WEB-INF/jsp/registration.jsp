<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<head>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/phones.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="<c:url value="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <link href="https://dimox.github.io/jQueryFormStyler/dist/jquery.formstyler.css" rel="stylesheet"/>
    <link href="https://dimox.github.io/jQueryFormStyler/dist/jquery.formstyler.theme.css" rel="stylesheet"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/phones.js"></script>
    <script src="https://dimox.github.io/jQueryFormStyler/dist/jquery.formstyler.js"></script>
    <script src="https://dimox.github.io/jQueryFormStyler/demo/demo.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/m.css">
</head>
<html>
<body id="tour" style="background-color:rgb(229, 224, 255);">
<div class="w3-container w3-content w3-white w3-card mycont" style="max-width:500px; ">
    <h2>Registration</h2>
    <form name="registration" action="/registration/save" method="post">
        <p><b>Имя:</b><br><c:if test="${not empty fields}">
        <p style="font-size: x-small; color: red; "><b>*</b></p>
        </c:if>
        <input class="styler" type="text" name="firstName" id="firstName" size="50" value="${account.firstName}"
               placeholder="Имя"></p>
        <p><b>Фамилия:</b><br><c:if test="${not empty fields}">
        <p style="font-size: x-small; color: red; "><b>*</b></p>
        </c:if>
        <input class="styler" type="text" name="secondName" id="secondName" size="50" value="${account.secondName}"
               placeholder="Фамилия"></p>
        <p><b>Отчество:</b><br>
            <input class="styler" type="text" name="middleName" id="middleName" size="50" value="${account.middleName}"
                   placeholder="Отчество">
        </p>
        <p><b>Email:</b><br>
            <input class="styler" type="text" name="email" id="email" size="50" value="${account.email}"
                   placeholder="email"></p>
        <p><b>Skype:</b><br>
            <input class="styler" type="text" name="skype" id="skype" size="50" value="${account.skype}"
                   placeholder="skype"></p>

        <p><b>Логин:</b><br><c:if test="${not empty fields}">
        <p style="font-size: x-small; color: red; "><b>*</b></p>
        </c:if>
        <c:if test="${not empty loginExist}">
            <p style="font-size: x-small; color: red; "><b>*</b></p>
        </c:if>
        <input class="styler" type="text" name="login" id="login" size="50" value="${account.login}"
               placeholder="Логин">

        <p><b>Пароль:</b><br><c:if test="${not empty fields}">
        <p style="font-size: x-small; color: red; "><b>*</b></p>
        </c:if>
        <input class="styler" type="password" name="password" id="password" size="50" placeholder="Пароль">
        </p>
        <p><b>Подтверждение пароля:</b><br><c:if test="${not empty fields}">
        <p style="font-size: x-small; color: red; "><b>*</b></p>
        </c:if>
        <input class="styler" type="password" name="repeatPassword" id="repeatPassword" size="50"
               placeholder="Подтверждение пароля"></p>
        <input class="ui-button ui-widget ui-corner-all w3-hover-blue" type="submit" value="Зарегистрироваться"/>
        <p style="font-size: x-small; color: red; ">${fields}${NotMatchPassword}</p>
    </form>
</div>
</body>
</html>