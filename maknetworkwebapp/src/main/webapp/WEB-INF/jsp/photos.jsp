<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<c:url value="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/phones.js"></script>
<link href="http://dimox.github.io/jQueryFormStyler/dist/jquery.formstyler.css" rel="stylesheet"/>
<link href="http://dimox.github.io/jQueryFormStyler/dist/jquery.formstyler.theme.css" rel="stylesheet"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<jsp:include page="/WEB-INF/jsp/upperPanel.jsp"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/m.css">
<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery.js"></script>
<body id="tour" style="background-color:rgb(229, 224, 255);">
<div class="w3-container w3-content w3-card w3-center">
    <div class="w3-card w3-container w3-row-padding w3-margin-top w3-centered" style="width:900px">
        <%--@elvariable id="photos" type="java.util.List"--%>
        <c:forEach items="${photos}" var="id">
            <div class="w3-quarter w3-card w3-centered mycont" style="padding: 20px; width:285px; height:350px">
                <image class="center-cropped" src="/image/${id.id}"></image>
                <a class="ui-button ui-widget ui-corner-all w3-hover-blue"
                   onclick="window.location='/photo/delete/${id.id}';">Удалить</a>
            </div>
        </c:forEach>
    </div>
</div>
</body>
</html>