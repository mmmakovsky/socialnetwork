<%--@elvariable id="account" type="com.getjavajob.training.web1805.makovskym.entities.Account"--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<c:url value="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link href="https://dimox.github.io/jQueryFormStyler/dist/jquery.formstyler.css" rel="stylesheet"/>
    <link href="https://dimox.github.io/jQueryFormStyler/dist/jquery.formstyler.theme.css" rel="stylesheet"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <jsp:include page="/WEB-INF/jsp/upperPanel.jsp"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/m.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery.js"></script>
</head>
<body id="tour" style="background-color:rgb(229, 224, 255);">
<div class="w3-container w3-content " id="tour2" style="max-width:900px">
    <div id="tour3" style="max-width:900px">
        <b></b>
        <div class="w3-row-padding w3-margin-top">
            <div class="w3-third w3-bordered">
                <div class="w3-card w3-container w3-centered w3-content mycont" style="background-color:rgb(255, 255, 255);
        height:300px">
                    <image class="center-cropped" src="/image/${account.mainPhoto.id}"></image>
                </div>
                <div class="w3-card w3-container w3-centered w3-content mycont"
                     style="background-color:rgb(255, 255, 255);">
                    <p></p>
                    <%--@elvariable id="homePage" type=""--%>
                    <c:if test="${homePage}">
                        <form action="photo/upload" method="post" enctype="multipart/form-data">
                            <input class="ui-button ui-widget ui-corner-all w3-hover-blue" type="button" name="click"
                                   onclick="document.getElementById('load').click()" value="Обновить фото"
                                   style="width: 230px">
                            <input style="display:none" id="load" type="file" name="file"
                                   onchange="document.getElementById('ww').click()" value="Select File"/>
                            <input style="display:none" name="ww" id="ww" type="submit" value="Upload File"/>
                        </form>
                    </c:if>
                    <c:if test="${!homePage}">
                        <form action="${pageContext.request.contextPath}/account/messages?id=${account.id}"
                              method="post">
                            <input class="ui-button ui-widget ui-corner-all w3-hover-blue" type="submit"
                                   value="Отправить сообщение" style="width: 230px"/>
                        </form>
                        <c:if test="${!friend}">
                            <form action="subscribe/add/${account.id}" method="post">
                                <input class="ui-button ui-widget ui-corner-all w3-hover-blue" type="submit"
                                       value="Дружить" style="width: 230px"/>
                            </form>
                        </c:if>
                    </c:if>
                    <%--@elvariable id="admin" type=""--%>
                    <c:if test="${!homePage && admin}">
                        <form action="admin/delete/${account.id}" method="post">
                            <input class="ui-button ui-widget ui-corner-all w3-hover-blue" type="submit"
                                   value="Удалить аккаунт" style="width: 230px"/>
                        </form>
                        <form action="make/admin/${account.id}" method="post">
                            <input class="ui-button ui-widget ui-corner-all w3-hover-blue" type="submit"
                                   value="Сделать администратором" style="width: 230px"/>
                        </form>
                    </c:if>
                </div>
                <br>
                <table class="w3-table w3-card mycont" style="background-color:rgb(255, 255, 255);">
                    <tbody>
                    <tr>
                        <th>Друзья</th>
                    </tr>
                    <c:forEach items="${account.friends}" var="friend">
                        <tr>
                            <td>
                                <div>
                                    <image src="/image/${friend.mainPhoto.id}" style="width:45px;height:50px;"></image>
                                    <a onclick="window.location='/account?id=${friend.id}';"
                                    >${friend.firstName} ${friend.secondName}</a>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <br>
                <table class="w3-card w3-table mycont"
                       style="background-color:rgb(255, 255, 255);">
                    <tbody>
                    <tr>
                        <th>Подписчики</th>
                    </tr>
                    <c:forEach items="${account.followers}" var="follower">
                        <tr>
                            <td>
                                <div>
                                    <image src="/image/${follower.mainPhoto.id}"
                                           style="width:45px;height:50px;"></image>
                                    <a onclick="window.location='/account?id=${follower.id}';">
                                            ${follower.firstName} ${follower.secondName}
                                    </a>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <br>
                <table class="w3-card w3-table mycont"
                       style="background-color:rgb(255, 255, 255);">
                    <tbody>
                    <tr>
                        <th>Заявки</th>
                    </tr>
                    <c:forEach items="${account.subscriptions}" var="follower">
                        <tr>
                            <td>
                                <div>
                                    <image src="/image/${follower.mainPhoto.id}"
                                           style="width:45px;height:50px;"></image>
                                    <a onclick="window.location='/account?id=${follower.id}';">${follower.firstName} ${follower.secondName}</a>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <br>
                <table class="w3-table w3-card mycont"
                       style="background-color:rgb(255, 255, 255);">
                    <tbody>
                    <tr>
                        <th>Группы</th>
                    </tr>
                    <c:forEach items="${account.groups}" var="group">
                        <tr>
                            <td>
                                <div>
                                    <image src="/image/${group.mainPhoto.id}" style="width:45px;height:50px;"></image>
                                    <a onclick="window.location='group/${group.id}';">${group.nameOfGroup}</a>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <br>
                <table class="w3-table w3-card mycont"
                       style="background-color:rgb(255, 255, 255);">
                    <tbody>
                    <tr>
                        <th>Подписки</th>
                    </tr>
                    <c:forEach items="${account.subscribedGroups}" var="group">
                        <tr>
                            <td>
                                <div>
                                    <image src="/image/${group.mainPhoto.id}" style="width:45px;height:50px;"></image>
                                    <a onclick="window.location='group/${group.id}';">${group.nameOfGroup}</a>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
            <div class="w3-twothird">
                <div class="w3-card mycont"
                     style="background-color:rgb(255, 255, 255);">
                    <div class="w3-container">
                        <h3><b>${account.firstName} ${account.secondName}</b></h3>
                    </div>
                    <hr>
                    <div class="w3-container">
                        <p>День рождения: ${account.birthday}</p>

                        <p>Email: ${account.email}</p>

                        <p>Skype: ${account.skype}</p>
                    </div>
                    <p id="flip" class="w3-btn" style="width:550px;" align="center">Показать/скрыть дополнительную
                        информацию</p>
                    <div id="panel">
                        <div class="w3-container">
                            <p>Личные телефоны:</p>
                            <table style="background-color:rgb(255, 255, 255);">
                                <tbody>
                                <c:forEach items="${account.homePhones}" var="phone">
                                    <tr>
                                        <td>
                                                ${phone.phoneNumber}
                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                        <div class="w3-container">
                            <p>Рабочие телефоны:</p>
                            <table style="background-color:rgb(255, 255, 255);">
                                <tbody>
                                <c:forEach items="${account.workPhones}" var="phone">
                                    <tr>
                                        <td>
                                                ${phone.phoneNumber}
                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                        <div class="w3-container ">
                            <p>icq: ${account.icq}</p>
                        </div>
                        <div class="w3-container">
                            <p>Домашний адрес: ${account.homeAddress}</p>
                        </div>
                        <div class="w3-container">
                            <p>Рабочий адрес: ${account.workAddress}</p>
                        </div>
                        <div class="w3-container">
                            <p><b>Дополнительная информация:</b></p>
                        </div>
                        <div class="w3-container">
                            <p>${account.moreInformation}</p>
                        </div>
                    </div>
                </div>
                <br>
                <table class="w3-table w3-card mycont" style="background-color:rgb(255, 255, 255);">
                    <tbody>
                    <tr>
                        <td>
                            <form name="message" action="/account/${account.id}/wall/message" method="post">
                                <textarea class="styler" name="msg" rows="5" cols="69" placeholder="..."></textarea>
                                <br>
                                <input class="ui-button ui-widget ui-corner-all w3-hover-blue w3-right" type="submit"
                                       value="Отправить"/>
                            </form>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <jsp:useBean id="accountWall" scope="request" type="java.util.List"/>
                <c:forEach items="${accountWall}" var="msg">
                    <p></p>
                    <table class="w3-table w3-card mycont" style="background-color:rgb(255, 255, 255);">
                        <tbody>
                        <tr>
                            <td>
                                <div style="padding: 15px;">
                                    <div class="w3-row-padding">
                                        <div class="w3-quarter" style="max-width:50px">
                                            <image src="/image/${msg.author.mainPhoto.id}"
                                                   style="width:45px;height:50px;"></image>
                                        </div>
                                        <div class="w3-half" style="width: 370px">
                                            <a onclick="window.location='/account?id=${msg.author.id}';">
                                                <b>${msg.author.firstName} ${msg.author.secondName}</b></a>
                                            <p style="font-size:60%;">${msg.date}</p>
                                        </div>
                                        <div class="w3-quarter" style="max-width:50px">
                                            <a class="w3-right w3-hover-blue" style="font-size:60%;"
                                               onclick="window.location='/message/${msg.id}/delete';">Удалить</a>
                                        </div>
                                    </div>
                                    <div style="white-space: pre-line; word-wrap: break-word; text-align: justify; width:470px">
                                        <span style=" white-space: pre-line; word-wrap: break-word; ">${msg.msg}</span>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </c:forEach>
            </div>
        </div>
    </div>
</div>
</body>
</html>