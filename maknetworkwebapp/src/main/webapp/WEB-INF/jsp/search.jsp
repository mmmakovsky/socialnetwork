<%--@elvariable id="substr" type="java.lang.String"--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
    <title>Поиск</title>
    <jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/upperPanel.jsp"/>
    <link href="https://dimox.github.io/jQueryFormStyler/dist/jquery.formstyler.css" rel="stylesheet"/>
    <link href="https://dimox.github.io/jQueryFormStyler/dist/jquery.formstyler.theme.css" rel="stylesheet"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://dimox.github.io/jQueryFormStyler/dist/jquery.formstyler.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://resources/demos/style.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/m.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery.js"></script>
</head>
<body style="background-color:rgb(229, 224, 255);" id="tour">
<hr>
<br>
<div id="tabs" class="w3-container w3-content mycont" style="width:500px;">
    <ul>
        <li><a href="#tabs-1">Аккаунты</a></li>
        <li><a href="#tabs-2">Группы</a></li>
    </ul>
    <br>
    <form id="searchOnPage" name="searchOnPage" method="POST">
        <input class="styler" type="text" id="sub" name="sub" value="${substr}" size="63"/>
    </form>
    <div id="tabs-1">
        <div class="w3-container w3-content w3-bordered" style="width:450px;">
            <table class="w3-table">
                <div class="w3-card">
                    <tbody id="div1">
                    </tbody>
                </div>
            </table>
            <div class="w3-row-padding w3-margin-top w3-centered">
                <div class="w3-quarter" style="width:80px; min-height: 30px">
                    <button class="ui-button ui-widget ui-corner-all w3-hover-blue w3-right" id="back">Назад</button>
                </div>
                <div class="w3-half w3-centered" style="width:240px;"><span style="text-align: center;"
                                                                            id="page"></span></div>
                <div class="w3-quarter" style="min-height: 30px; width:80px;">
                    <button class="ui-button ui-widget ui-corner-all w3-hover-blue w3-right" id="next">Вперед</button>
                </div>
            </div>
        </div>
    </div>
    <div id="tabs-2">
        <div class="w3-container w3-content w3-bordered" style="width:450px;">
            <table class="w3-table">
                <div class="w3-card">
                    <tbody id="div1gr">
                    </tbody>
                </div>
            </table>
            <div class="w3-row-padding w3-centered w3-margin-top">
                <div class="w3-quarter" style="width:80px;min-height: 30px">
                    <button class="ui-button ui-widget ui-corner-all w3-hover-blue w3-right" id="backgr">Назад</button>
                </div>
                <div class="w3-half w3-centered" style="width:240px;"><span id="pagegr"></span></div>
                <div class="w3-quarter" style="width:80px; min-height: 30px">
                    <button class="ui-button ui-widget ui-corner-all w3-hover-blue w3-right" id="nextgr">Вперед</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/pagination.js">
</script>
</body>
</html>