<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
    <title>Social network</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/searchAjax.js"></script>
</head>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<form id="logoutForm" method="POST" action="${contextPath}/logout">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
</form>
<div class="w3-card-2 topnav" id="topnav" style="position: relative;">
    <div style="overflow:auto;">
        <div class="w3-bar w3-blue-grey w3-card" style="height:40px;">
            <a class="w3-btn w3-hover-blue" onclick="window.location='${contextPath}/account';" title="Home
        page">Домой</a>
            <a class="w3-btn w3-hover-blue" onclick="window.location='${contextPath}/account/messages';"
               title="Messages">Сообщения</a>
            <a class="w3-btn w3-hover-blue" onclick="window.location='${contextPath}/account/friends';"
               title="Friends list">Друзья</a>
            <a class="w3-btn w3-hover-blue " onclick="window.location='${contextPath}/account/groups';"
               title="Groups list">Группы</a>
            <a class="w3-btn w3-hover-blue " onclick="window.location='${contextPath}/account/photos';" title="Photo">Фотографии</a>
            <a class="w3-btn w3-hover-blue " onclick="window.location='${contextPath}/account/update';"
               title="Edit info about account">Редактировать</a>
            <a class="w3-btn w3-hover-blue " onclick="document.forms['logoutForm'].submit()" title="Logout">Выйти</a>
            <form class="w3-hide-small w3-right" method="GET" id="serching" name="searchController"
                  action="${contextPath}/account/search">
                <a href="#" class="w3-padding-large w3-hover-blue w3-hide-small w3-right"
                   onclick="document.searchController.submit(); return false;"><i class="fa fa-search"></i></a>
                <input class="text w3-hide-small w3-right w3-centered" type="text" id="substr" name="substr"
                       value="" size="30" placeholder=" Поиск" style="width: 50%; margin: 7px; outline: none;">
            </form>
        </div>
    </div>
</div>
</html>