<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head style="background-color:Grey;">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/m.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<c:url value="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/phones.js"></script>
    <link href="https://dimox.github.io/jQueryFormStyler/dist/jquery.formstyler.css" rel="stylesheet"/>
    <link href="https://dimox.github.io/jQueryFormStyler/dist/jquery.formstyler.theme.css" rel="stylesheet"/>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery.js"></script>
</head>
<body id="tour" style="background-color:rgb(229, 224, 255);">
<hr>
<div class="w3-container w3-content w3-white w3-card mycont" style="max-width:300px;">
    <h2>Авторизация</h2>
    <c:if test="${not empty param.error}">
        <span style="font-size: x-small; color: red; "><b>Неправильный логин или пароль</b></span>
    </c:if>
    <hr>
    <form name="search" action="<c:url value="/j_spring_security_check"/>" method="post">
        <input class="styler" type="text" name="username" id="login" size="31" placeholder="Username">
        <input class="styler" type="password" name="password" id="password" size="31" placeholder="Password">
        <p class="remember_me">
        <td><input type="checkbox" name="remember-me"/> Запомнить меня</td>

        <p>
            <input class="ui-button ui-widget ui-corner-all w3-hover-blue" type="submit" value="Войти"
                   style="width: 268px">
    </form>

    <p><input class="ui-button ui-widget ui-corner-all w3-hover-blue" type="submit" value="Зарегистрироваться"
              onclick="window.location='registration';" style="width: 268px"/>
    </p>
</div>
</body>
</html>