<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head lang="ru">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Ошибка 500</title>
    <jsp:include page="/WEB-INF/jsp/upperPanel.jsp"/>
</head>
<body style="background-color:rgb(229, 224, 255);">
<hr>
<div class="w3-container w3-content w3-white w3-card mycont" style="max-width:300px;">
    <h2 class="headline text-red"> 500</h2>
    <div class="error-content" style="padding-top: 20px">
        <h3><i class="fa fa-warning text-red"></i> Что-то пошло не так</h3>
        <p>
            К сожалению, на сервере произошла ошибка
        </p>
    </div>
</div>
</body>
</html>