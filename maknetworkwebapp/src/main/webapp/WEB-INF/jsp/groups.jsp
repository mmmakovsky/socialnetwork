<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/m.css">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://resources/demos/style.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery.js"></script>
    <jsp:include page="/WEB-INF/jsp/upperPanel.jsp"/>
</head>
<hr>
<body style="background-color:rgb(229, 224, 255);" id="tour">
<hr>
<br>
<div id="tabs" class="w3-container w3-content mycont" style="width:500px;">
    <ul>
        <li><a href="#tabs-1">Группы</a></li>
        <li><a href="#tabs-2">Подписки</a></li>
        <li class="w3-left"><a class="ui-button ui-widget ui-corner-all w3-hover-blue w3-right"
                               onclick="window.location='/creating/group';" title="creatingGroup">
            Создать группу
        </a></li>
    </ul>
    <br>
    <div id="tabs-1">
        <div class="w3-container w3-content w3-bordered" style="width:450px;">
            <table class="w3-table">
                <div class="w3-card">
                    <tbody>
                    <%--@elvariable id="groups" type="java.util.List"--%>
                    <c:forEach items="${groups}" var="group">
                        <tr>
                            <td>
                                <div>
                                    <image src="/image/${group.mainPhoto.id}" style="width:45px;height:50px;"></image>
                                    <a onclick="window.location='/group/${group.id}';">
                                            ${group.nameOfGroup}
                                    </a>
                                    <form class="w3-hide-small w3-right" method="post"
                                          action="${pageContext.request.contextPath}/group/${group.id}/follower/delete">
                                        <input class="ui-button ui-widget ui-corner-all  w3-hover-blue" type="submit"
                                               value="Покинуть группу"/>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </div>
            </table>
        </div>
    </div>
    <div id="tabs-2">
        <div class="w3-container w3-content w3-bordered" style="width:450px;">
            <table class="w3-table">
                <div class="w3-card">
                    <tbody>
                    <%--@elvariable id="subscribedGroups" type="java.util.List"--%>
                    <c:forEach items="${subscribedGroups}" var="group">
                        <tr>
                            <td>
                                <div>
                                    <image src="/image/${group.mainPhoto.id}" style="width:45px;height:50px;"></image>
                                    <a onclick="window.location='/group/${group.id}';">
                                            ${group.nameOfGroup}
                                    </a>
                                    <form class="w3-hide-small w3-right"
                                          action="${pageContext.request.contextPath}/group/${group.id}/follower/delete"
                                          method="post">
                                        <input class="ui-button ui-widget ui-corner-all  w3-hover-blue" type="submit"
                                               value="Удалить заявку"/>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </div>
            </table>
        </div>
    </div>
</div>
</body>
</html>
