<%--@elvariable id="user" type="com.getjavajob.training.web1805.makovskym.entities.Account"--%>
<html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/upperPanel.jsp"/>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/sockjs-client/1.1.4/sockjs.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/stomp.js/2.3.3/stomp.min.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<title>Login</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/main.css"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/m.css">
<body id="tour" style="background-color:rgb(229, 224, 255);">
<div class="w3-container w3-content " style="max-width:900px">
    <div style="max-width:900px">
        <b></b>
        <div class="w3-row-padding w3-margin-top">
            <div class="w3-third mycont" style="background-color:rgb(255, 255, 255);">
                <table class="w3-table " style="background-color:rgb(255, 255, 255);">
                    <tbody>
                    <div class="w3-card">
                        <tr>
                            <th>Диалоги</th>
                        </tr>
                        <%--@elvariable id="dialogues" type="java.util.List"--%>
                        <c:forEach items="${dialogues}" var="dialogue">
                            <tr>
                                <td><a class="w3-btn" style="width:220px;"
                                       onclick="window.location='/account/messages?id=${dialogue.id}';">
                                    <div class="w3-left">
                                        <image src="/image/${dialogue.mainPhoto.id}"
                                               style="width:45px;height:50px;"></image>
                                            ${dialogue.firstName} ${dialogue.secondName}
                                    </div>
                                </a></td>
                            </tr>
                        </c:forEach>
                    </div>
                    </tbody>
                </table>
                <form id="loginForm" name="loginForm" method="POST">
                    <input type="hidden" id="username" name="username" value="${user.firstName} ${user.secondName}"/>
                </form>
            </div>
            <%--@elvariable id="haveDialogue" type="java.lang.Boolean"--%>
            <c:if test="${haveDialogue}"><%--@elvariable id="dialog" type="java.lang.Long"--%>
                <%--@elvariable id="friend" type="com.getjavajob.training.web1805.makovskym.entities.Account"--%>
                <div class="w3-twothird">
                    <div id="chat-container">
                        <div class="w3-container">
                            <h4>${friend.firstName} ${friend.secondName}</h4>
                        </div>
                        <hr/>
                        <ul id="messageArea">
                                <%--@elvariable id="messages" type="java.util.List"--%>
                            <c:forEach items="${messages}" var="msg">
                                <li class="chat-message">
                                    <strong class="nickname"> ${msg.author.firstName} ${msg.author.secondName}</strong>
                                    <p style="font-size:60%;">${msg.date}</p>
                                    <span style="text-align: justify;">${msg.msg} </span>
                                </li>
                            </c:forEach>
                        </ul>
                        <form id="messageForm" name="messageForm">
                            <div class="input-message">
                                <textarea id="message" name="message" rows="5" cols="48" placeholder="..."></textarea>
                                <label for="dest"></label><input class="w3-right" id="dest" name="dest"
                                                                 value="${friend.id}" style="display:none"/>
                                <label for="dialog"></label><input class="w3-right" id="dialog" name="dialog"
                                                                   value="${dialog}"
                                                                   style="display:none"/>
                                <button class="ui-button ui-widget ui-corner-all w3-right">Отправить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </c:if>
        </div>
    </div>
</div>
<script src="${pageContext.request.contextPath}/static/js/messages.js"></script>
</body>
</html>