<%--@elvariable id="group" type="com.getjavajob.training.web1805.makovskym.entities.Group"--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<c:url value="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link href="https://dimox.github.io/jQueryFormStyler/dist/jquery.formstyler.css" rel="stylesheet"/>
<link href="https://dimox.github.io/jQueryFormStyler/dist/jquery.formstyler.theme.css" rel="stylesheet"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/phones.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/m.css">
<link href="https://dimox.github.io/jQueryFormStyler/dist/jquery.formstyler.css" rel="stylesheet"/>
<link href="https://dimox.github.io/jQueryFormStyler/dist/jquery.formstyler.theme.css" rel="stylesheet"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery.js"></script>
<jsp:include page="/WEB-INF/jsp/upperPanel.jsp"/>
<body id="tour" style="background-color:rgb(229, 224, 255);">
<div class="w3-container w3-content " id="tour2" style="max-width:900px">
    <div id="tour3" style="max-width:900px">
        <b></b>
        <div class="w3-row-padding w3-margin-top">
            <div class="w3-twothird">
                <div class="w3-card mycont"
                     style="background-color:rgb(255, 255, 255); height:300px">
                    <div class="w3-container">
                        <h1>${group.nameOfGroup}</h1>
                    </div>
                    <hr>
                    <div class="w3-container">
                        <p>${group.groupDescription}</p>
                    </div>
                </div>
                <br>
                <%--@elvariable id="inGroup" type="java.lang.Boolean"--%>
                <%--@elvariable id="request" type="java.lang.Boolean"--%>
                <c:if test="${inGroup&&!request}">
                    <table class="w3-table w3-card mycont" style="background-color:rgb(255, 255, 255);">
                        <tbody>
                        <tr>
                            <td>
                                <form name="message" action="/group/${group.id}/wall/message" method="post">
                                    <textarea class="styler" name="msg" rows="5" cols="56" placeholder="..."></textarea>
                                    <input class="ui-button ui-widget ui-corner-all w3-hover-blue w3-right"
                                           type="submit" value="Send"/>
                                </form>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <%--@elvariable id="groupWall" type="java.util.List"--%>
                    <c:forEach items="${groupWall}" var="msg">
                        <p></p>
                        <table class="w3-table w3-card mycont" style="background-color:rgb(255, 255, 255);">
                            <tbody>
                            <tr>
                                <td>
                                    <div style="padding: 15px;">
                                        <div class="w3-row-padding">
                                            <div class="w3-quarter" style="max-width:50px">
                                                <image src="/image/${msg.author.mainPhoto.id}"
                                                       style="width:45px;height:50px;"></image>
                                            </div>

                                            <div class="w3-half" style="width: 370px">
                                                <a onclick="window.location='/account?id=${msg.author.id}';">
                                                    <b>${msg.author.firstName} ${msg.author.secondName}</b></a>
                                                <p style="font-size:60%;">${msg.date}</p>
                                            </div>
                                            <div class="w3-quarter" style="max-width:50px">
                                                <a class="w3-right w3-hover-blue" style="font-size:60%;"
                                                   onclick="window.location='/message/${msg.id}/delete';">Удалить</a>
                                            </div>
                                        </div>
                                        <div style=" white-space: pre-line; word-wrap: break-word; text-align: justify; width:450px">
                                            <span style=" white-space: pre-line; word-wrap: break-word; ">${msg.msg}</span>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </c:forEach>
                </c:if>
            </div>
            <div class="w3-third w3-bordered">
                <div class="w3-card w3-container w3-centered w3-content mycont" style="background-color:rgb(255, 255, 255);
        height:300px">
                    <image class="center-cropped" src="/image/${group.mainPhoto.id}"></image>
                </div>
                <div class="w3-card w3-container w3-centered w3-centered w3-content mycont"
                     style="background-color:rgb(255, 255, 255);">
                    <p></p>
                    <%--@elvariable id="admin" type="java.lang.Boolean"--%>
                    <c:if test="${admin}">
                        <form action="/group/${group.id}/photo/upload" method="post" enctype="multipart/form-data">
                            <input class="ui-button ui-widget ui-corner-all w3-hover-blue" type="button" name="click"
                                   onclick="document.getElementById('load').click()" value="Обновить фото"
                                   style="width: 230px">
                            <input style="display:none" id="load" type="file" name="file"
                                   onchange="document.getElementById('ww').click()" value="Select File"/>
                            <input style="display:none" name="ww" id="ww" type="submit" value="Upload File"/>
                        </form>
                        <form action="/group/${group.id}/edit" method="post">
                            <input class="ui-button ui-widget ui-corner-all w3-hover-blue" type="submit"
                                   value="Редактировать"
                                   style="width: 230px"/>
                        </form>
                    </c:if>
                    <c:if test="${!inGroup}">
                        <form action="/request/${group.id}/add" method="post">
                            <input class="ui-button ui-widget ui-corner-all w3-hover-blue" type="submit"
                                   value="Подать заявку" style="width: 230px"/>
                        </form>
                    </c:if>
                    <c:if test="${inGroup}">
                        <form action="/group/${group.id}/follower/delete" method="post">
                            <input class="ui-button ui-widget ui-corner-all w3-hover-blue" type="submit"
                                   value="Отписаться" style="width: 230px"/>
                        </form>
                    </c:if>
                </div>
                <br>
                <table class="w3-table w3-card mycont" style="background-color:rgb(255, 255, 255);">
                    <tbody>
                    <tr>
                        <th>Администраторы</th>
                    </tr>
                    <c:forEach items="${group.admins}" var="friend">
                        <tr>
                            <td>
                                <div>
                                    <image src="/image/${friend.mainPhoto.id}"
                                           style="width:45px;height:50px;"></image>
                                    <a onclick="window.location='/account?id=${friend.id}';">
                                            ${friend.firstName} ${friend.secondName}
                                    </a>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <br>
                <table class="w3-card w3-table mycont" style="background-color:rgb(255, 255, 255);">
                    <tbody>
                    <tr>
                        <th>Подписчики</th>
                    </tr>
                    <c:forEach items="${group.followers}" var="follower">
                        <tr>
                            <td>
                                <div>
                                    <image src="/image/${follower.mainPhoto.id}"
                                           style="width:45px;height:50px;"></image>
                                    <a onclick="window.location='/account?id=${follower.id}';">${follower.firstName} ${follower.secondName}</a>
                                </div>
                                <div>
                                    <c:if test="${admin}">
                                    <div class="w3-row-padding w3-margin-top">
                                        <div class="w3-half">
                                            <input class="w3-right" name="followerID" value="${follower.id}"
                                                   style="display:none"/>
                                        </div>
                                        <div class="w3-half">
                                            <form name="message" action="/group/${group.id}/follower/delete"
                                                  method="post">
                                                <input class="w3-right" name="followerId" value="${follower.id}"
                                                       style="display:none"/>
                                                <input class="ui-button ui-widget ui-corner-all w3-hover-blue"
                                                       type="submit" value="delete"/>
                                            </form>
                                        </div>
                                        </c:if>
                                    </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <br>
                <table class="w3-card w3-table mycont"
                       style="background-color:rgb(255, 255, 255);">
                    <tbody>
                    <tr>
                        <th>Заявки</th>
                    </tr>
                    <c:if test="${admin}">
                        <c:forEach items="${group.requests}" var="follower">
                            <tr>
                                <td>
                                    <div>
                                        <image src="/image/${follower.mainPhoto.id}"
                                               style="width:45px;height:50px;"></image>
                                        <a onclick="window.location='/account?id=${follower.id}';">${follower.firstName} ${follower.secondName}</a>
                                    </div>
                                    <div class="w3-row-padding w3-margin-top">
                                        <div class="w3-half">
                                            <form name="message" action="/follower/${group.id}/add" method="post">
                                                <input class="w3-right" name="followerID" value="${follower.id}"
                                                       style="display:none"/>
                                                <input class="ui-button ui-widget ui-corner-all w3-hover-blue"
                                                       type="submit" value="add"/>
                                            </form>
                                        </div>
                                        <div class="w3-half">
                                            <form name="message" action="/group/${group.id}/follower/delete"
                                                  method="post">
                                                <input class="w3-right" name="followerId"
                                                       value="${follower.id}"
                                                       style="display:none"/>
                                                <input class="ui-button ui-widget ui-corner-all w3-hover-blue"
                                                       type="submit" value="delete"/>
                                            </form>
                                        </div>
                                    </div>

                                </td>
                            </tr>
                        </c:forEach>
                    </c:if>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<br>
<br>
</body>
</html>