package com.getjavajob.training.web1805.makovskym.controllers;

import com.getjavajob.training.web1805.makovskym.AccountService;
import com.getjavajob.training.web1805.makovskym.GroupService;
import com.getjavajob.training.web1805.makovskym.MessageService;
import com.getjavajob.training.web1805.makovskym.SecurityUtils;
import com.getjavajob.training.web1805.makovskym.entities.Account;
import com.getjavajob.training.web1805.makovskym.entities.Group;
import com.getjavajob.training.web1805.makovskym.entities.Message;
import com.getjavajob.training.web1805.makovskym.entities.Photo;
import com.getjavajob.training.web1805.makovskym.enums.RoleInGroup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;

@Controller
@SessionAttributes("user")
public class GroupController {
    private static final String REDIRECT = "redirect:";
    private static final String REFERER = "referer";
    private static final Logger logger = LoggerFactory.getLogger(GroupController.class);
    private final AccountService accountService;
    private final GroupService groupService;
    private final MessageService messageService;

    @Autowired
    public GroupController(AccountService accountService, GroupService groupService, MessageService messageService) {
        this.accountService = accountService;
        this.groupService = groupService;
        this.messageService = messageService;
    }

    @RequestMapping("/group/create")
    public String createGroup(@ModelAttribute Group group) {
        group.setCreatedDate(new Date(Calendar.getInstance().getTimeInMillis()));
        Account creator = SecurityUtils.getCurrentUser();
        group.setCreator(creator);
        group.setMainPhoto(accountService.getPhoto(2L));
        groupService.createGroup(group);
        groupService.addFollower(group, creator);
        groupService.updateRole(group, creator, RoleInGroup.ADMIN);
        return "redirect:/group/" + group.getId();
    }

    @RequestMapping("/group/{id}/edition")
    public String createGroup(@PathVariable("id") Long id, @RequestParam("nameOfGroup") String name,
                              @RequestParam("groupDescription") String description) {
        Group group = groupService.getGroup(id);
        group.setNameOfGroup(name);
        group.setGroupDescription(description);
        groupService.updateGroup(group);
        return "redirect:/group/" + group.getId();
    }

    @RequestMapping("/group/{id}")
    public ModelAndView showGroup(@PathVariable("id") Long id) {
        Group group = groupService.getGroup(id);
        List<Message> groupWall = messageService.getGroupWall(group);
        Account user = SecurityUtils.getCurrentUser();
        RoleInGroup role = groupService.getRole(group, user);
        boolean inGroup = role != null;
        boolean request = inGroup && role.equals(RoleInGroup.REQUEST);
        boolean admin = inGroup && role.equals(RoleInGroup.ADMIN);
        String viewName = "group";
        ModelAndView modelAndView = new ModelAndView(viewName);
        modelAndView.addObject(viewName, group);
        modelAndView.addObject("groupWall", groupWall);
        modelAndView.addObject("admin", admin);
        modelAndView.addObject("inGroup", inGroup);
        modelAndView.addObject("request", request);
        return modelAndView;
    }

    @RequestMapping("/creating/group")
    public ModelAndView creatingGroup() {
        ModelAndView modelAndView = new ModelAndView("creatingGroup");
        modelAndView.addObject("title", "Создать группу");
        modelAndView.addObject("path", "/group/create");
        return modelAndView;
    }

    @RequestMapping("/follower/{id}/add")
    public String addFollower(HttpServletRequest request, @PathVariable("id") Long groupId, @RequestParam("followerID") Long id) {
        Group group = groupService.getGroup(groupId);
        Account follower = accountService.getAccount(id);
        groupService.updateRole(group, follower, RoleInGroup.FOLLOWER);
        return REDIRECT + request.getHeader(REFERER);
    }

    @RequestMapping("/request/{id}/add")
    public String addRequest(HttpServletRequest request, @PathVariable("id") Long groupId) {
        Group group = groupService.getGroup(groupId);
        Account account = SecurityUtils.getCurrentUser();
        groupService.addRequest(group, account);
        return REDIRECT + request.getHeader(REFERER);
    }

    @RequestMapping("/group/{id}/follower/delete")
    public String deleteFollower(HttpServletRequest request, @PathVariable(value = "id") Long groupId, @RequestParam(value = "followerId", required = false) Long id) {
        Group group = groupService.getGroup(groupId);
        Account follower;
        if (id != null) {
            follower = accountService.getAccount(id);
        } else {
            follower = SecurityUtils.getCurrentUser();
        }
        groupService.deleteFollower(group, follower);
        return REDIRECT + request.getHeader(REFERER);
    }

    @RequestMapping("/group/{id}/photo/upload")
    public String uploadGroupPhoto(HttpServletRequest request, @RequestPart("file") MultipartFile file, @PathVariable("id") Long id) {
        Group group = groupService.getGroup(id);
        Photo photo = new Photo();
        try {
            photo.setImage(file.getBytes());
        } catch (IOException e) {
            logger.error("Exception: ", e);
        }
        Account account = SecurityUtils.getCurrentUser();
        photo.setAuthor(account);
        accountService.setPhoto(photo);
        group.setMainPhoto(photo);
        groupService.updateGroup(group);
        logger.info("upload group photo end");
        return REDIRECT + request.getHeader(REFERER);
    }

    @RequestMapping("/get/group/list/size")
    @ResponseBody
    public int search(@RequestParam("substr") String substring) {
        return groupService.getGroupsBuSubstr(substring).size();
    }

    @RequestMapping("/get/group/list")
    @ResponseBody
    public List<Group> searchBySubstrings(@RequestParam("substr") String substring, @RequestParam("fromIndex") int fromIndex,
                                          @RequestParam("toIndex") int toIndex) {
        List<Group> accounts = groupService.getGroupsBuSubstr(substring);
        return accounts.subList(fromIndex, toIndex);
    }

    @RequestMapping("/group/{id}/edit")
    public ModelAndView editGroup(@PathVariable("id") Long groupId) {
        Group group = groupService.getGroup(groupId);
        ModelAndView modelAndView = new ModelAndView("creatingGroup");
        modelAndView.addObject("group", group);
        modelAndView.addObject("title", "Редактировать");
        modelAndView.addObject("path", "/group/" + groupId + "/edition");
        return modelAndView;
    }
}
