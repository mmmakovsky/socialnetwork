package com.getjavajob.training.web1805.makovskym.controllers;

import com.getjavajob.training.web1805.makovskym.entities.ChatMessage;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;

@Controller
public class WebSocketController {

    @MessageMapping("/chat.sendMessage/{chat}")
    @SendTo("/topic/publicChatRoom/{chat}")
    public ChatMessage sendMessage(@Payload ChatMessage chatMessage, @DestinationVariable Long chat) {
        return chatMessage;
    }

    @MessageMapping("/chat.addUser/{chat}")
    public ChatMessage addUser(@Payload ChatMessage chatMessage, SimpMessageHeaderAccessor headerAccessor) {
        if (headerAccessor.getSessionAttributes() != null) {
            headerAccessor.getSessionAttributes().put("username", chatMessage.getSender());
        }
        return chatMessage;
    }
}