package com.getjavajob.training.web1805.makovskym.controllers;

import com.getjavajob.training.web1805.makovskym.AccountService;
import com.getjavajob.training.web1805.makovskym.SecurityUtils;
import com.getjavajob.training.web1805.makovskym.entities.Account;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;

@SuppressWarnings("ConstantConditions")
@Controller
public class FriendshipsController {
    private static final String REDIRECT = "redirect:";
    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);
    private AccountService accountService;

    @Autowired
    public FriendshipsController(AccountService accountService) {
        this.accountService = accountService;
    }

    @RequestMapping("/friend/add/{id}")
    public String addFriend(@PathVariable("id") Long id, @RequestHeader("referer") String referer) {
        Account user = SecurityUtils.getCurrentUser();
        Account friend = accountService.getAccount(id);
        accountService.addFriend(user, friend);
        user.getFriends().add(friend);
        logger.info("Friend added, end addFriend");
        logger.info(referer);
        return REDIRECT + referer;
    }

    @RequestMapping("/subscribe/add/{id}")
    public String addSubscribe(@PathVariable("id") Long id, @RequestHeader("referer") String referer) {
        Account user = SecurityUtils.getCurrentUser();
        Account friend = accountService.getAccount(id);
        accountService.followUser(user, friend);
        user.getSubscriptions().add(friend);
        logger.info("follow user");
        logger.info(referer);
        return REDIRECT + referer;
    }

    @RequestMapping("/friend/delete/{id}")
    public String deleteFriend(@PathVariable("id") Long id, Model model, @RequestHeader("referer") String referer) {
        logger.info("start delete friend");
        Account user = SecurityUtils.getCurrentUser();
        Account friend = accountService.getAccount(id);
        accountService.deleteFriend(user, friend);
        user.getFriends().remove(friend);
        logger.info("end delete friend");
        return REDIRECT + referer;
    }

    @RequestMapping("/follower/delete/{id}")
    public String deleteFollower(@PathVariable("id") Long id, @RequestHeader("referer") String referer) {
        logger.info("start delete follower");
        Account user = SecurityUtils.getCurrentUser();
        Account follower = accountService.getAccount(id);
        accountService.deleteFollower(user, follower);
        user.getFollowers().remove(follower);
        logger.info("end delete follower");
        return REDIRECT + referer;
    }

    @RequestMapping("/subscribe/delete/{id}")
    public String deleteSubscribe(@PathVariable("id") Long id, @RequestHeader("referer") String referer) {
        logger.info("start delete friend");
        Account user = SecurityUtils.getCurrentUser();
        Account friend = accountService.getAccount(id);
        accountService.deleteFollower(friend, user);
        user.getSubscriptions().remove(friend);
        logger.info("end delete friend");
        logger.info(referer);
        return REDIRECT + referer;
    }
}
