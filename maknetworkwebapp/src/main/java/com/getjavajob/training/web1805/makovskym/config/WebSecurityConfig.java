package com.getjavajob.training.web1805.makovskym.config;

import com.getjavajob.training.web1805.makovskym.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    public UserDetailsServiceImpl userDetailsService;


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .rememberMe()
                .rememberMeCookieName("remember-me")
                .tokenValiditySeconds(1209600);
        http.authorizeRequests()
                .antMatchers("/", "/login", "/logout").permitAll();
        http.authorizeRequests()
                .antMatchers("/account").hasAnyAuthority("ROLE_USER", "ROLE_ADMIN");
        http.authorizeRequests().and().exceptionHandling().accessDeniedPage("/403");
        http.formLogin()
                .loginProcessingUrl("/j_spring_security_check").permitAll()
                .failureForwardUrl("/login?err")
                .loginPage("/login")
                .defaultSuccessUrl("/account")
                .usernameParameter("username")
                .passwordParameter("password");
        http.sessionManagement()
                .sessionAuthenticationErrorUrl("/login?err=auth");
        http.logout()
                .permitAll()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/login?logout")
                .invalidateHttpSession(true)
                .deleteCookies("JSESSIONID")
                .deleteCookies("remember-me");
    }

    @Bean
    public PasswordEncoder passwordEncoderBean() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    public void registerGlobalAuthentication(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoderBean());
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}