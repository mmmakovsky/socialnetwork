package com.getjavajob.training.web1805.makovskym.controllers;

import com.getjavajob.training.web1805.makovskym.*;
import com.getjavajob.training.web1805.makovskym.entities.*;
import com.getjavajob.training.web1805.makovskym.enums.AccountRoles;
import com.getjavajob.training.web1805.makovskym.enums.PhoneType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@SuppressWarnings("ConstantConditions")
@Controller
public class AccountController {
    private static final String ACCOUNT_ATTRIBUTE = "account";
    private static final String REDIRECT = "redirect:";
    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);
    private AccountService accountService;
    private GroupService groupService;
    private MessageService messageService;
    private SecurityService securityService;

    @Autowired
    public AccountController(AccountService accountService, GroupService groupService, MessageService messageService,
                             SecurityService securityService) {
        this.accountService = accountService;
        this.groupService = groupService;
        this.messageService = messageService;
        this.securityService = securityService;
    }

    @RequestMapping(value = "/account")
    private ModelAndView showAccount(@RequestParam(value = "id", required = false) Long id) {
        logger.info("Start showAccount with id {} ", id);
        Account homeUser = SecurityUtils.getCurrentUser();
        ModelAndView modelAndView = new ModelAndView("/accountPage");
        //noinspection ConstantConditions
        boolean homePage = id == null || id.equals(homeUser.getId());
        Account account;
        if (homePage) {
            account = homeUser;
        } else {
            account = accountService.getAccount(id);
            boolean friend = homeUser.getFriends().contains(account) || homeUser.getSubscriptions().contains(account);
            modelAndView.addObject("friend", friend);
        }
        List<Message> accountWall = messageService.getAccountWall(account);
        boolean admin = homeUser.getAccountRole().equals(AccountRoles.ADMIN);
        modelAndView.addObject(ACCOUNT_ATTRIBUTE, account);
        modelAndView.addObject("accountWall", accountWall);
        modelAndView.addObject("homePage", homePage);
        modelAndView.addObject("admin", admin);
        logger.info("End showAccount, home page {}", homePage);
        return modelAndView;
    }

    @RequestMapping(value = "/account/update")
    public ModelAndView showUpdateAccount() {
        Account account = SecurityUtils.getCurrentUser();
        logger.debug("Start showUpdateAccount with userId {}", account.getId());
        ModelAndView modelAndview = new ModelAndView("edit");
        modelAndview.addObject(ACCOUNT_ATTRIBUTE, account);
        logger.info("End showUpdateAccount");
        return modelAndview;
    }

    @RequestMapping(value = {"/login", "/"})
    public ModelAndView login(ModelAndView model, String logout, String err) {
        if (logout != null) {
            logger.debug("logout true");
            model.addObject("message", "Logged out successfully.");
        }
        if (err != null) {
            model.addObject("error", "Неверный пароль");
        }
        logger.info("Go to login page");
        model.setViewName("login");
        return model;
    }

    @RequestMapping("/photo/delete/{id}")
    public String deletePhoto(@PathVariable("id") Long id, @RequestHeader("referer") String referer) {
        accountService.deletePhoto(id);
        return REDIRECT + referer;
    }

    @RequestMapping(value = "/edit")
    public String updateAccount(@ModelAttribute Account account, @RequestParam("birth") String birth,
                                @RequestParam(value = "ph", required = false) String phones) {
        Account user = SecurityUtils.getCurrentUser();
        java.sql.Date birthday = java.sql.Date.valueOf(birth);
        account.setBirthday(birthday);
        account.setId(user.getId());
        String[] ph = phones.split(",");
        List<Phone> homePhones = new ArrayList<>();
        List<Phone> workPhones = new ArrayList<>();
        for (int i = 0; i < ph.length; i += 2) {
            if (ph[i + 1].equals("HOME")) {
                homePhones.add(new Phone(ph[i], account, PhoneType.HOME));
            } else {
                workPhones.add(new Phone(ph[i], account, PhoneType.WORK));
            }
        }
        account.setHomePhones(homePhones);
        account.setWorkPhones(workPhones);
        accountService.updateAccount(account);
        logger.info("Account updated");
        return "redirect:/account?id=" + account.getId();
    }

    @RequestMapping(value = "/account/edit/password")
    public ModelAndView updatePassword(@RequestParam("Old Password") String oldPassword,
                                       @RequestParam("New Password") String newPassword,
                                       @RequestParam("Repeat new password") String repeatPassword) {
        ModelAndView modelAndView = new ModelAndView("/edit");
        Account account = SecurityUtils.getCurrentUser();
        if (account.getPassword().equals(oldPassword)) {
            if (newPassword.equals(repeatPassword)) {
                accountService.updatePassword(account, newPassword);
                modelAndView.addObject("successfully", "Password changed successfully");
            } else {
                modelAndView.addObject("NotMatchPassword", "Passwords do not match, try again");
            }
        } else {
            modelAndView.addObject("IncorrectPassword", "Incorrect password, try again");
        }
        return modelAndView;
    }

    @RequestMapping(value = "/account/delete")
    public String deleteAccount() {
        Account account = SecurityUtils.getCurrentUser();
        accountService.deleteAccount(account);
        return "redirect:/logout";
    }

    @RequestMapping(value = "/admin/delete/{id}")
    public String deleteAcauntAsAdmin(@PathVariable("id") Long id) {
        Account account = accountService.getAccount(id);
        accountService.deleteAccount(account);
        return "redirect:/account?id=";
    }

    @RequestMapping(value = "/make/admin/{id}")
    public String makeAdmin(@RequestParam("id") Long id) {
        Account account = accountService.getAccount(id);
        accountService.updateRole(account, AccountRoles.ADMIN);
        return "redirect:/account" + account.getId();
    }

    @RequestMapping(value = "/registration/save")
    public ModelAndView registration(@ModelAttribute Account account, @RequestParam("repeatPassword") String repeatPassword) {
        logger.debug("start registration controller");
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject(ACCOUNT_ATTRIBUTE, account);
        String view = "registration";
        if (accountService.getAccountByLogin(account.getLogin()) != null) {
            modelAndView.addObject("loginExist", "Пользователь с таким огином уже существует!");
            modelAndView.setViewName(view);
            return modelAndView;
        }
        if (account.getFirstName().equals("") || account.getSecondName().equals("") || account.getLogin().equals("")
                || account.getPassword().equals("")) {
            modelAndView.addObject("fields", "Заполните обязательные поля");
            modelAndView.setViewName(view);
            return modelAndView;
        }
        if (account.getPassword().equals(repeatPassword)) {
            account.setRegistrationDate(new java.sql.Date(Calendar.getInstance().getTimeInMillis()));
            account.setAccountRole(AccountRoles.USER);
            account.setMainPhoto(accountService.getPhoto(1L));
            PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            String cryptPassword = passwordEncoder.encode(account.getPassword());
            account.setPassword(cryptPassword);
            accountService.createAccount(account);
            securityService.autoLogin(account.getUsername(), repeatPassword);
            return showAccount(account.getId());
        } else {
            modelAndView.addObject("NotMatchPassword", "Пароли не совпадают");
            modelAndView.setViewName(view);
            return modelAndView;
        }
    }


    @RequestMapping("/account/friends")
    public ModelAndView showFriends() {
        Account account = SecurityUtils.getCurrentUser();
        ModelAndView modelAndView = new ModelAndView("friends");
        modelAndView.addObject("friends", account.getFriends());
        modelAndView.addObject("followers", account.getFollowers());
        modelAndView.addObject("subscribes", account.getSubscriptions());
        return modelAndView;
    }

    @RequestMapping("/account/groups")
    public ModelAndView showGroups() {
        Account account = SecurityUtils.getCurrentUser();
        ModelAndView modelAndView = new ModelAndView("groups");
        modelAndView.addObject("groups", account.getGroups());
        modelAndView.addObject("subscribedGroups", account.getSubscribedGroups());
        return modelAndView;
    }

    @RequestMapping("/account/messages")
    public ModelAndView showMessages(@RequestParam(value = "id", required = false) Long id) {
        Account account = SecurityUtils.getCurrentUser();
        logger.debug("Start showMessages");
        List<Account> dialogues = messageService.getDialogues(account);
        List<Message> messages = new ArrayList<>();
        boolean haveDialogue = false;
        ModelAndView modelAndView = new ModelAndView("messages");
        if (id != null) {
            haveDialogue = true;
            Account friend = accountService.getAccount(id);
            messages = messageService.getDialogue(account, friend);
            modelAndView.addObject("friend", friend);
        }
        modelAndView.addObject("dialogues", dialogues);
        modelAndView.addObject("messages", messages);
        modelAndView.addObject("user", account);
        modelAndView.addObject("haveDialogue", haveDialogue);
        if (id != null) {
            Long dd = messageService.getDialogueId(id, account.getId());
            modelAndView.addObject("dialog", dd);
        }
        logger.info("End showMessages");
        return modelAndView;
    }

    @RequestMapping("/registration")
    public ModelAndView registrationControl() {
        ModelAndView modelAndView = new ModelAndView("registration");
        modelAndView.addObject(ACCOUNT_ATTRIBUTE, new Account());
        return modelAndView;
    }

    @RequestMapping("/photo/upload")
    public ModelAndView uploadAccount(@RequestPart("file") MultipartFile file) {
        Account account = SecurityUtils.getCurrentUser();
        logger.info("start upload");
        Photo photo = new Photo();
        try {
            photo.setImage(file.getBytes());
        } catch (IOException e) {
            logger.error("file/upload Exception: ", e);
        }
        photo.setAuthor(account);
        accountService.setPhoto(photo);
        account.setMainPhoto(photo);
        accountService.updatePhoto(account);
        logger.info("end uploaded");
        return showAccount(account.getId());
    }

    @RequestMapping("/account/photos")
    public ModelAndView photosController() {
        Account account = SecurityUtils.getCurrentUser();
        List<Photo> photos = accountService.getAllAccountPhotos(account);
        ModelAndView modelAndView = new ModelAndView("photos");
        modelAndView.addObject("photos", photos);
        return modelAndView;
    }

    @RequestMapping("/image/{id}")
    public void imageController(HttpServletResponse response, @PathVariable(value = "id", required = false) Long id) {
        logger.info("image start {}", id);
        Photo img = accountService.getPhoto(id);
        if (img != null) {
            response.setContentType("image/gif");
            try (OutputStream o = response.getOutputStream()) {
                o.write(img.getImage());
                o.flush();
            } catch (IOException e) {
                logger.error("IOException: ", e);
            }
        }
    }

    @RequestMapping("/account/search")
    public ModelAndView showSearch(@RequestParam("substr") String substring) {
        ModelAndView modelAndView = new ModelAndView("search");
        modelAndView.addObject("substr", substring);
        return modelAndView;
    }

    @RequestMapping("/searching")
    @ResponseBody
    public List<Object> searchBySubstring(@RequestParam("substr") String substring) {
        List<Object> objects = new ArrayList<>();
        List<Account> accounts = accountService.getBySubstring(substring);
        List<Group> groups = groupService.getGroupsBuSubstr(substring);
        if (groups.size() > 5) {
            groups = groups.subList(0, 5);
        }
        if (accounts.size() > 5) {
            accounts = accounts.subList(0, 5);
        }
        objects.addAll(accounts);
        objects.addAll(groups);
        return objects;
    }

    @RequestMapping("/get/account/list/size")
    @ResponseBody
    public int search(@RequestParam("substr") String substring) {
        int count = accountService.getBySubstring(substring).size();
        logger.trace("go to jsp search");
        return count;
    }

    @RequestMapping("/get/account/list")
    @ResponseBody
    public List<Account> searchBySubstrings(@RequestParam("substr") String substring, @RequestParam("fromIndex") int fromIndex,
                                            @RequestParam("toIndex") int toIndex) {
        List<Account> accounts = accountService.getBySubstring(substring);
        return accounts.subList(fromIndex, toIndex);
    }

    @RequestMapping("/account/download")
    @ResponseBody
    public String downloadAccount(@RequestParam("id") Long id) {
        logger.info("download account {} ", id);
        Account account = accountService.getAccount(id);
        String fileName = "C:/downloads/" + account.getFirstName() + '_' + account.getSecondName() + ".xml";
        try {
            JAXBContext context = JAXBContext.newInstance(Account.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.marshal(account, new File(fileName));
            logger.info("file saved!");
        } catch (JAXBException e) {
            logger.error("Exception JAXB: ", e);
        }
        return fileName;
    }

    @RequestMapping("/xml/upload")
    public ModelAndView uploadXmlAccount(@RequestPart("file") MultipartFile file) {
        Account account = new Account();
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Account.class);
            Unmarshaller un = jaxbContext.createUnmarshaller();
            account = (Account) un.unmarshal(file.getInputStream());
        } catch (JAXBException | IOException e) {
            logger.error("Exception:  ", e);
        }
        ModelAndView modelAndview = new ModelAndView("edit");
        modelAndview.addObject(ACCOUNT_ATTRIBUTE, account);
        logger.info("End showUpdateAccount");
        return modelAndview;
    }

    @RequestMapping("/403")
    public String error403() {
        return "403";
    }

    @RequestMapping("/404")
    public String error404() {
        return "404";
    }

    @RequestMapping("/500")
    public String error500() {
        return "500";
    }
}
