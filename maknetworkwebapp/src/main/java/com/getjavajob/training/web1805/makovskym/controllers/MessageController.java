package com.getjavajob.training.web1805.makovskym.controllers;

import com.getjavajob.training.web1805.makovskym.AccountService;
import com.getjavajob.training.web1805.makovskym.GroupService;
import com.getjavajob.training.web1805.makovskym.MessageService;
import com.getjavajob.training.web1805.makovskym.SecurityUtils;
import com.getjavajob.training.web1805.makovskym.entities.Account;
import com.getjavajob.training.web1805.makovskym.entities.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.Date;

@Controller
public class MessageController {
    private static final Logger logger = LoggerFactory.getLogger(MessageController.class);
    @Autowired
    private AccountService accountService;
    @Autowired
    private GroupService groupService;
    @Autowired
    private MessageService messageService;

    @RequestMapping("/account/{dest}/wall/message")
    public String sendAccountWallMessage(@RequestParam("msg") String msg,
                                         @PathVariable("dest") Long dest) {
        logger.debug("Start sendAccountWallMsg");
        Message message = new Message();
        message.setMsg(msg);
        logger.trace("msg: " + msg);
        logger.trace("dest: " + dest);
        Account author = SecurityUtils.getCurrentUser();
        message.setAuthor(author);
        message.setDestinationId(dest);
        message.setDate(new Timestamp(new Date().getTime()));
        message.setPhotoID(null);
        logger.trace("Go to service createAccountWallMessage");
        messageService.createAccountWallMessage(message);
        logger.info("Message sande, end sendAccountWallMsg");
        return "redirect:/account?id=" + dest;
    }

    @RequestMapping("/group/{dest}/wall/message")
    public String sendGroupWallMessage(@RequestParam("msg") String msg,
                                       @PathVariable("dest") Long dest) {
        Message message = new Message();
        message.setMsg(msg);
        Account author = SecurityUtils.getCurrentUser();
        message.setAuthor(author);
        message.setDestinationId(dest);
        message.setDate(new Timestamp(new Date().getTime()));
        message.setPhotoID(null);
        messageService.createGroupWallMessage(message);
        return "redirect:/group/" + dest;
    }

    @RequestMapping("/account/private/message")
    public void sendMessageChat(@RequestParam("content") String msg,
                                @RequestParam("dest") Long dest, @RequestParam("dtime") String time) {
        logger.info("Ajax create message chat");
        logger.error("msg" + msg);
        Message message = new Message();
        message.setMsg(msg);
        Account author = SecurityUtils.getCurrentUser();
        message.setAuthor(author);
        message.setDestinationId(dest);
        message.setDate(new Timestamp(new Date().getTime()));
        message.setPhotoID(null);
        messageService.createPrivateMessage(message);
        logger.error("message inserts ");
    }

    @RequestMapping("/message/{id}/delete")
    public String deleteMessage(@PathVariable("id") Long id, HttpServletRequest request) {
        messageService.deleteMessage(id);
        logger.info("delete Message");
        return "redirect:" + request.getHeader("referer");
    }
}
