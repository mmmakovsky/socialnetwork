var messageForm = document.querySelector('#messageForm');
var messageInput = document.querySelector('#message');
var messageArea = document.querySelector('#messageArea');
var connectingElement = document.querySelector('#connecting');
var stompClient = null;
var username = null;

$(document).ready(function () {
    messageArea.scrollTop = messageArea.scrollHeight;
});

function connect() {
    username = document.forms["loginForm"].elements["username"].value;
    var socket = new SockJS('/ws');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, onConnected, onError);
}

connect();

function onConnected() {
    var dialog = document.forms["messageForm"].elements["dialog"].value;
    stompClient.subscribe('/topic/publicChatRoom/' + dialog, onMessageReceived);
    stompClient.send("/app/chat.addUser/" + dialog,
        {},
        JSON.stringify({sender: username, type: 'JOIN'})
    );
    connectingElement.classList.add('hidden');
}

function onError(error) {
    connectingElement.textContent = 'Could not connect to WebSocket server. Please refresh this page to try again!';
    connectingElement.style.color = 'red';
}

function sendMessage(event) {
    var dialog = document.forms["messageForm"].elements["dialog"].value;
    var messageContent = messageInput.value.trim();
    var datetime = new Date();
    var dest = document.forms["messageForm"].elements["dest"].value;
    if (messageContent && stompClient) {
        var chatMessage = {
            sender: username,
            dtime: datetime,
            content: messageInput.value,
            dest: dest,
            type: 'CHAT'
        };
        stompClient.send("/app/chat.sendMessage/" + dialog, {}, JSON.stringify(chatMessage));
    }
    $.ajax({
        url: '/account/private/message',
        data: {
            dtime: datetime,
            content: messageInput.value,
            dest: dest
        }
    });
    messageInput.value = '';
    event.preventDefault();
}

function onMessageReceived(payload) {
    var message = JSON.parse(payload.body);
    var messageElement = document.createElement('li');
    messageElement.classList.add('chat-message');
    var usernameElement = document.createElement('strong');
    usernameElement.classList.add('nickname');
    var usernameText = document.createTextNode(message.sender);
    usernameElement.appendChild(usernameText);
    messageElement.appendChild(usernameElement);
    var dateElement = document.createElement('p');
    dateElement.setAttribute("style", "font-size:60%;");
    var d = new Date(message.dtime);
    var dateTime = document.createTextNode(d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " +
        d.getHours() + ":" + d.getMinutes());
    dateElement.appendChild(dateTime);
    var textElement = document.createElement('span');
    var messageText = document.createTextNode(message.content);
    textElement.appendChild(messageText);
    messageElement.appendChild(dateElement);
    messageElement.appendChild(textElement);
    messageArea.appendChild(messageElement);
    messageArea.scrollTop = messageArea.scrollHeight;
}

messageForm.addEventListener('submit', sendMessage, true);