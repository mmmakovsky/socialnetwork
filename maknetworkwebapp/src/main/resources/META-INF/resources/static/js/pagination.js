$(document).ready(function () {
    var totalRecords;
    var recordsPerPage = 5;
    var recordsToFetch = recordsPerPage;
    var totalPages;
    var currentPage = 1;
    var currentIndex = 0;
    var ss = document.forms["searchOnPage"].elements["sub"].value;
    $.ajax({
        url: '/get/account/list/size',
        data: {
            substr: ss
        },
        success: function (result) {
            totalRecords = result;
            totalPages = Math.floor(totalRecords / recordsPerPage);
            if (totalRecords % recordsPerPage !== 0) {
                totalPages++;
            }
            if (totalRecords < recordsPerPage) {
                recordsToFetch = totalRecords % recordsPerPage;
            }
            else {
                recordsToFetch = recordsPerPage;
            }
            if (totalRecords === 0) {
                $("#page").html("Не найдено ни одной записи, удоветворяющей запросу");
                $("#back").hide();
                $("#next").hide();
            } else {
                $("#page").html("Страница " + currentPage + " из " + totalPages);

                $.ajax({
                    url: '/get/account/list',
                    data: {
                        substr: document.forms["searchOnPage"].elements["sub"].value,
                        fromIndex: currentIndex,
                        toIndex: currentIndex + recordsToFetch
                    },
                    success: function (result) {
                        $.map(result, function (item) {
                            $("#div1").append("<tr><td><div><image src=\"/image/" + item.mainPhoto.id + "\"style=\"width:45px;height:50px;\"/><a onclick='window.location=\"/account?id=" + item.id + "\"'; >"
                                + item.firstName + ' ' + item.secondName + "</a></div></td></tr>");
                            currentIndex++;
                            return {value: item.id, label: item.firstName + ' ' + item.secondName}
                        });
                        if (currentPage === totalPages) {
                            $("#next").hide();
                        }
                        else {
                            $("#next").show();
                        }
                        if (currentPage === 1) {
                            $("#back").hide();
                        }
                        else {
                            $("#back").show();
                        }
                    }
                });
            }
        }
    });

    $("#next").click(function () {
        $("#div1").html("");
        currentPage++;
        if (currentPage === totalPages) {
            if (totalRecords % recordsPerPage !== 0) {
                recordsToFetch = totalRecords % recordsPerPage;
            }
            else {
                recordsToFetch = recordsPerPage;
            }
            $("#next").hide();
        }
        else {
            recordsToFetch = recordsPerPage;
            $("#next").show();
        }
        if (currentPage === 1) {
            $("#back").hide();
        }
        else {
            $("#back").show();
        }
        $.ajax({
            url: '/get/account/list',
            data: {
                substr: document.forms["searchOnPage"].elements["sub"].value,
                fromIndex: currentIndex,
                toIndex: currentIndex + recordsToFetch
            },
            success: function (result) {
                $.map(result, function (item) {
                    $("#div1").append("<tr><td><div><image src=\"/image/" + item.mainPhoto.id + "\"style=\"width:45px;height:50px;\"/>  <a onclick='window.location=\"/account?id=" + item.id + "\"'; >"
                        + ' ' + item.firstName + ' ' + item.secondName + "</a></div></td></tr>");
                    currentIndex++;
                    return {value: item.id, label: item.firstName + ' ' + item.secondName}
                });
            }
        });
        $("#page").html("Страница " + currentPage + " из " + totalPages);
    });

    $("#back").click(function () {
        $("#div1").html("");
        currentPage--;
        currentIndex = currentIndex - recordsToFetch - recordsPerPage;
        if (currentPage === totalPages) {
            $("#next").hide();
            recordsToFetch = totalRecords % recordsPerPage;
        }
        else {
            $("#next").show();
            recordsToFetch = recordsPerPage;
        }
        if (currentPage === 1) {
            $("#back").hide();
        }
        else {
            $("#back").show();
        }

        $.ajax({
            url: '/get/account/list',
            data: {
                substr: document.forms["searchOnPage"].elements["sub"].value,
                fromIndex: currentIndex,
                toIndex: currentIndex + recordsToFetch
            },
            success: function (result) {
                $.map(result, function (item) {
                    $("#div1").append("<tr><td><div><image src=\"/image/" + item.mainPhoto.id + "\"style=\"width:45px;height:50px;\"/>  <a onclick='window.location=\"account?id=" + item.id + "\"'; >"
                        + ' ' + item.firstName + ' ' + item.secondName + "</a></div></td></tr>");
                    currentIndex++;
                    return {value: item.id, label: item.firstName + ' ' + item.secondName}
                });
            }
        });
        $("#page").html("Страница " + currentPage + " из " + totalPages);
    });
});

$(document).ready(function () {
    var totalRecordsgr;
    var recordsPerPagegr = 5;
    var recordsToFetchgr;
    var totalPagesgr;
    var currentPagegr = 1;
    var currentIndexgr = 0;
    var ssgr = document.forms["searchOnPage"].elements["sub"].value;
    $.ajax({
        url: '/get/group/list/size',
        data: {
            substr: ssgr
        },
        success: function (result) {
            totalRecordsgr = result;
            totalPagesgr = Math.floor(totalRecordsgr / recordsPerPagegr);
            recordsToFetchgr = 1;
            if (totalRecordsgr % recordsPerPagegr !== 0) {
                totalPagesgr++;
            }
            if (totalRecordsgr < recordsPerPagegr) {
                recordsToFetchgr = totalRecordsgr % recordsPerPagegr;
            }
            else {
                recordsToFetchgr = recordsPerPagegr;
            }
            if (totalRecordsgr === 0) {
                $("#pagegr").html("Не найдено ни одной записи, удоветворяющей запросу");
                $("#backgr").hide();
                $("#nextgr").hide();
            } else {
                $("#pagegr").html("Страница " + currentPagegr + " из " + totalPagesgr);
                $.ajax({
                    url: '/get/group/list',
                    data: {
                        substr: ssgr,
                        fromIndex: currentIndexgr,
                        toIndex: currentIndexgr + recordsToFetchgr
                    },
                    success: function (result) {
                        $.map(result, function (item) {
                            $("#div1gr").append("<tr><td><div><image src=\"/image/" + item.mainPhoto.id + "\"style=\"width:45px;height:50px;\"/>  <a onclick='window.location=\"/group/" + item.id + "\"'; >"
                                + ' ' + item.nameOfGroup + "</a></div></td></tr>");
                            currentIndexgr++;
                            return {value: item.id}
                        });
                        if (currentPagegr === totalPagesgr) {
                            $("#nextgr").hide();
                        }
                        else {
                            $("#nextgr").show();
                        }
                        if (currentPagegr === 1) {
                            $("#backgr").hide();
                        }
                        else {
                            $("#backgr").show();
                        }
                    }
                });
            }
        }
    });

    $("#nextgr").click(function () {
        $("#div1gr").html("");
        currentPagegr++;
        if (currentPagegr === totalPagesgr) {
            if (totalRecordsgr % recordsPerPagegr !== 0) {
                recordsToFetchgr = totalRecordsgr % recordsPerPagegr;
            }
            else {
                recordsToFetchgr = recordsPerPagegr;
            }
            $("#nextgr").hide();
        }
        else {
            recordsToFetchgr = recordsPerPagegr;
            $("#nextgr").show();
        }
        if (currentPagegr === 1) {
            $("#backgr").hide();
        }
        else {
            $("#backgr").show();
        }
        $.ajax({
            url: '/get/group/list',
            data: {
                substr: ssgr,
                fromIndex: currentIndexgr,
                toIndex: currentIndexgr + recordsToFetchgr
            },
            success: function (result) {
                $.map(result, function (item) {
                    $("#div1gr").append("<tr><td><div><image src=\"/image/" + item.mainPhoto.id + "\"style=\"width:45px;height:50px;\"/> <a onclick='window.location=\"/group/" + item.id + "\"'; >"
                        + ' ' + item.nameOfGroup + "</a></div></td></tr>");
                    currentIndexgr++;
                    return {value: item.id}
                });
            }
        });
        $("#pagegr").html("Страница " + currentPagegr + " из " + totalPagesgr);
    });

    $("#backgr").click(function () {
        $("#div1gr").html("");
        currentPagegr--;
        currentIndexgr = currentIndexgr - recordsToFetchgr - recordsPerPagegr;
        if (currentPagegr === totalPagesgr) {
            $("#nextgr").hide();
            recordsToFetchgr = totalRecordsgr % recordsPerPagegr;
        }
        else {
            $("#nextgr").show();
            recordsToFetchgr = recordsPerPagegr;
        }
        if (currentPagegr === 1) {
            $("#backgr").hide();
        }
        else {
            $("#backgr").show();
        }

        $.ajax({
            url: '/get/group/list',
            data: {
                substr: ssgr,
                fromIndex: currentIndexgr,
                toIndex: currentIndexgr + recordsToFetchgr
            },
            success: function (result) {
                $.map(result, function (item) {
                    $("#div1gr").append("<tr><td><div><image src=\"/image/" + item.mainPhoto.id + "\"style=\"width:45px;height:50px;\"/><a onclick='window.location=\"/group/" + item.id + "\"'; >"
                        + item.nameOfGroup + "</a></div></td></tr>");
                    currentIndexgr++;
                    return {value: item.id}
                });
            }
        });
        $("#pagegr").html("Страница " + currentPagegr + " из " + totalPagesgr);
    });
});