$(document).ready(function () {
    $("#flip").click(function () {
        $("#panel").slideToggle("slow");
    });
    $("#panel").hide();
});

$(function () {
    $("#tabs").tabs();
});

$(function ($) {
    $(document).ready(function () {
        $('input, select').styler({
            selectSearch: true
        });
    });
})(jQuery);

$(function () {
    $(".widget input[type=submit], .widget a, .widget button").button();
    $("button, input, a").click(function (event) {
        event.preventDefault();
    });
});

