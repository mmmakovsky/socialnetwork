$(document).ready(function () {
    $("#substr").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: '/searching',
                data: {
                    substr: request.term
                },
                success: function (data) {
                    response($.map(data, function (item) {
                        if (typeof item.nameOfGroup !== 'undefined') {
                            return {value: item.id, label: item.nameOfGroup, type: "gr"}
                        } else {
                            return {value: item.id, label: item.firstName + ' ' + item.secondName, type: "acc"}
                        }
                    }));
                }
            });
        },
        minLength: 1,
        select: function (event, ui) {
            if (ui.item.type !== "gr") {
                location.href = "<c:url value='/account?id='/>" + ui.item.value;
            } else {
                location.href = '<c:url value="/group/"/>' + ui.item.value;
            }

            return false;
        }
    });
});