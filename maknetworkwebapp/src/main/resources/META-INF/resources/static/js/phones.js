function loadFromXml() {
    $.ajax({
        type: "GET",
        url: "/account/upload",
        dataType: "xml",
        success: function (xml) {
            $(xml).find('account').each(function () {
                $("#firstName").val($(this).find('firstName').text());
                $("#secondName").val($(this).find('secondName').text());
                $("#middleName").val($(this).find('middleName').text());
                $("#email").val($(this).find('email').text());
                $("#homeAddress").val($(this).find('homeAddress').text());
                $("#workAddress").val($(this).find('workAddress').text());
                $("#skype").val($(this).find('skype').text());
                $("#icq").val($(this).find('icq').text());
                $("#moreInformation").val($(this).find('moreInformation').text());
            });
        }
    });
}

function downloadFile() {
    $.ajax({
        url: "/account/download",
        data: {
            id: document.forms["edit"].elements["id"].value
        }, success: function (result) {
            alert("Инфомация об аккаунте сохранена в файл: " + result);
        }
    });
}

function confirmChanges() {
    if (confirm("Сохранить изменения?")) {
        return testform.submit();
    } else {
        return false;
    }
}

function ValidPhone() {
    document.getElementById('message').innerHTML = '';
    var re = /\+\d\(\d{3}\)\d{7}/;
    var myPhone = document.getElementById('input0').value;
    var phoneType = document.getElementById('input1').value;
    var valid = re.test(myPhone) && !(phoneType !== 'undefined' && phoneType.trim() === '');
    if (valid) {
        addRow('phones');
    } else if (!re.test(myPhone)) {
        document.getElementById('message').innerHTML = 'Неверный формат! введите номер в формате +7(999)9999999 ';
    } else {
        document.getElementById('message').innerHTML = 'Select type!';
    }
    return valid;
}

function addRow(tableID) {
    var table = document.getElementById(tableID);

    var colCount = table.rows[0].cells.length;
    var validate_Noof_columns = (colCount - 1);
    for (var j = 0; j < colCount; j++) {
        if (j === validate_Noof_columns) {
            var row = table.insertRow(1);
            var phone = window.document.getElementById('input0').value;
            var phoneType = window.document.getElementById('input1').value;
            var newcell0 = row.insertCell(0);
            var phoneInput = "<input  type='hidden'  name='ph' id='ph' value='" + phone + "'><p style='width:180px;'>" + phone + "</p>";
            newcell0.innerHTML = phoneInput;
            window.document.getElementById('input0').value = '';
            var newcell1 = row.insertCell(1);
            var typeInput = "<input  type='hidden'  name='ph' id='ph' value='" + phoneType + "'><p style='width:100px;'>" + phoneType + "</p>";
            newcell1.innerHTML = typeInput;
            var newcell2 = row.insertCell(2);
            newcell2.innerHTML = "<INPUT style='width:45px;'' class='styler' type='button' value='X' onclick='removeRow(this)'/>";
        }
    }
}

function removeRow(onclickTAG) {
    while ((onclickTAG = onclickTAG.parentElement) && onclickTAG.tagName !== 'TR') ;
    onclickTAG.parentElement.removeChild(onclickTAG);
}

(function ($) {
    $(function () {
        $('input, select').styler({
            selectSearch: true
        });
    });
})(jQuery);
