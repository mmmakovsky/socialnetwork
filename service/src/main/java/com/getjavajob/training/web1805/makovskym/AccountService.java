package com.getjavajob.training.web1805.makovskym;

import com.getjavajob.training.web1805.makovskym.entities.Account;
import com.getjavajob.training.web1805.makovskym.entities.Friendships;
import com.getjavajob.training.web1805.makovskym.entities.Photo;
import com.getjavajob.training.web1805.makovskym.enums.AccountRoles;
import com.getjavajob.training.web1805.makovskym.enums.FriendsRelationships;
import com.getjavajob.training.web1805.makovskym.impl.AccountDaoImpl;
import com.getjavajob.training.web1805.makovskym.impl.PhoneRepository;
import com.getjavajob.training.web1805.makovskym.keys.FriendshipsKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AccountService {
    private static final Logger logger = LoggerFactory.getLogger(AccountService.class);
    private AccountDao accountDao;
    private PhoneRepository phoneDao;

    @Autowired
    public AccountService(AccountDaoImpl accountDao, PhoneRepository phoneDao) {
        this.accountDao = accountDao;
        this.phoneDao = phoneDao;
    }

    @Transactional
    public void createAccount(Account account) {
        logger.info("create account service");
        accountDao.insertAccount(account);
    }

    @Transactional
    @Secured(value = {"admin"})
    public void deleteAccount(Account account) {
        logger.info("delete account service");
        accountDao.deleteAccount(account);
    }

    @Transactional(readOnly = true)
    public Photo getPhoto(Long id) {
        logger.info("get photo {}", id);
        if (id == null) {
            return accountDao.getPhoto(1L);
        } else {
            return accountDao.getPhoto(id);
        }
    }

    @Transactional(readOnly = true)
    public List<Photo> getAllAccountPhotos(Account account) {
        logger.info("get all photos");
        return accountDao.getAccountPhotos(account.getId());
    }

    @Transactional
    public void updateAccount(Account account) {
        logger.info("Start update account {} service ", account.getId());
        phoneDao.deleteByAccount(account);
        logger.trace("All phones deleted");
        phoneDao.saveAll(account.getHomePhones());
        phoneDao.saveAll(account.getWorkPhones());
        accountDao.updateAccount(account);
    }

    @Transactional
    public void setPhoto(Photo photo) {
        logger.info("set info service ");
        accountDao.insertPhoto(photo);
    }

    @Transactional
    public void updatePhoto(Account account) {
        logger.info("update account service");
        accountDao.updateAccount(account);
    }

    @Transactional(readOnly = true)
    public List<Account> getBySubstring(String substr) {
        logger.info("get account by substr {} service", substr);
        return accountDao.getBySubstring(substr);
    }

    @Transactional(readOnly = true)
    public Account getAccount(Long id) {
        logger.info("get account {} service", id);
        return accountDao.get(id);
    }

    @Transactional(readOnly = true)
    public Account getAccountByLogin(String login) {
        logger.info("get account by login  {}  service", login);
        return accountDao.getByLogin(login);
    }


    @Transactional
    public void followUser(Account user, Account friend) {
        logger.info("follow user service");
        accountDao.insertFriend(new Friendships(friend.getId(), user.getId(), FriendsRelationships.FOLLOWER));
    }

    @Transactional
    public void addFriend(Account user, Account friend) {
        logger.info("add friend service");
        accountDao.insertFriend(new Friendships(friend.getId(), user.getId(), FriendsRelationships.FRIEND));
        accountDao.updateFriendship(new FriendshipsKey(user.getId(), friend.getId()));
    }

    @Transactional
    public void deleteFriend(Account user, Account friend) {
        logger.info("delete friend service");
        FriendshipsKey key = new FriendshipsKey(user.getId(), friend.getId());
        accountDao.deleteFriend(key);
        FriendshipsKey secondKey = new FriendshipsKey(friend.getId(), user.getId());
        accountDao.deleteFriend(secondKey);
    }

    @Transactional
    public void deleteSubscribe(Account user, Account friend) {
        logger.info("delete subscribe service");
        FriendshipsKey key = new FriendshipsKey(friend.getId(), user.getId());
        accountDao.deleteFriend(key);
    }

    @Transactional
    public void deleteFollower(Account user, Account friend) {
        logger.info("delete follower service");
        FriendshipsKey key = new FriendshipsKey(user.getId(), friend.getId());
        accountDao.deleteFriend(key);
    }

    @Transactional
    public void deletePhoto(Long photo) {
        accountDao.deletePhoto(photo);
        logger.info("photo deleted");
    }

    @Transactional
    public void updateRole(Account account, AccountRoles role) {
        logger.info("update role");
        account.setAccountRole(role);
        accountDao.updateAccount(account);
    }

    @Transactional
    public void updatePassword(Account account, String password) {
        logger.info("update password service");
        account.setPassword(password);
        accountDao.updateAccount(account);
    }
}
