package com.getjavajob.training.web1805.makovskym;

import com.getjavajob.training.web1805.makovskym.entities.Account;
import com.getjavajob.training.web1805.makovskym.entities.Group;
import com.getjavajob.training.web1805.makovskym.entities.Memberships;
import com.getjavajob.training.web1805.makovskym.enums.RoleInGroup;
import com.getjavajob.training.web1805.makovskym.impl.GroupRepository;
import com.getjavajob.training.web1805.makovskym.impl.MembershipsDaoImpl;
import com.getjavajob.training.web1805.makovskym.keys.MembershipsKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class GroupService {
    private static final Logger logger = LoggerFactory.getLogger(SecurityService.class);

    private GroupRepository groupDao;
    private MembershipsDaoImpl membershipsDao;

    @Autowired
    public GroupService(GroupRepository groupDao, MembershipsDaoImpl membershipsDao) {
        this.groupDao = groupDao;
        this.membershipsDao = membershipsDao;
    }

    @Transactional
    public void createGroup(Group group) {
        logger.info("save group service");
        groupDao.save(group);
    }

    @Transactional
    public void addFollower(Group group, Account account) {
        logger.info("add follower service");
        Memberships memberships = new Memberships();
        memberships.setAccountId(account.getId());
        memberships.setGroupId(group.getId());
        memberships.setRole(RoleInGroup.FOLLOWER);
        membershipsDao.insertFollower(memberships);
    }

    @Transactional
    public void addRequest(Group group, Account account) {
        logger.info("add requet service");
        Memberships memberships = new Memberships();
        memberships.setAccountId(account.getId());
        memberships.setGroupId(group.getId());
        memberships.setRole(RoleInGroup.REQUEST);
        membershipsDao.insertFollower(memberships);
    }

    @Transactional
    public void updateGroup(Group group) {
        logger.info("save group service");
        groupDao.save(group);
    }

    @Transactional
    public void updateRole(Group group, Account account, RoleInGroup role) {
        logger.info("update role service");
        Memberships memberships = new Memberships();
        memberships.setAccountId(account.getId());
        memberships.setGroupId(group.getId());
        memberships.setRole(role);
        membershipsDao.updateRole(memberships);
    }

    public Group getGroup(Long id) {
        logger.info("get group service ");
        return groupDao.findById(id).orElse(null);
    }

    @Transactional
    public List<Group> getGroupsBuSubstr(String substr) {
        logger.info("get groups by substr {}", substr);
        return groupDao.findAllByNameOfGroup(substr);
    }

    public RoleInGroup getRole(Group group, Account account) {
        logger.info("get role group service");
        return membershipsDao.getRole(group.getId(), account.getId());
    }

    @Transactional
    public void deleteGroup(Group group) {
        logger.info("delete group service");
        groupDao.delete(group);
    }

    @Transactional
    public void deleteFollower(Group group, Account account) {
        logger.info("delete follower service");
        membershipsDao.deleteFollower(new MembershipsKey(group.getId(), account.getId()));
    }
}
