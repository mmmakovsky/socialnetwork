package com.getjavajob.training.web1805.makovskym;

import com.getjavajob.training.web1805.makovskym.entities.Account;
import com.getjavajob.training.web1805.makovskym.entities.Dialogues;
import com.getjavajob.training.web1805.makovskym.entities.Group;
import com.getjavajob.training.web1805.makovskym.entities.Message;
import com.getjavajob.training.web1805.makovskym.enums.MessageType;
import com.getjavajob.training.web1805.makovskym.impl.MessageDaoImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class MessageService {
    private static final Logger logger = LoggerFactory.getLogger(MessageService.class);
    private MessageDao messageDao;

    @Autowired
    public MessageService(MessageDaoImpl messageDao) {
        this.messageDao = messageDao;
    }

    @Transactional
    public void createPrivateMessage(Message msg) {
        logger.info("insert privae message service");
        msg.setType(MessageType.PRIVATE);
        messageDao.insert(msg);
    }

    @Transactional
    public void createAccountWallMessage(Message msg) {
        logger.info("Start createAccountWallMessage");
        msg.setType(MessageType.PUBLIC);
        messageDao.insert(msg);
        logger.info("End createAccountWallMessage");
    }

    @Transactional
    public void createGroupWallMessage(Message msg) {
        logger.info("insert group whall msg ");
        msg.setType(MessageType.GROUP);
        messageDao.insert(msg);
    }

    @Transactional
    public List<Message> getAccountWall(Account account) {
        logger.info("get account whall service");
        return messageDao.getAccountWall(account.getId());
    }

    @Transactional
    public List<Message> getGroupWall(Group group) {
        logger.info("get group wall service");
        return messageDao.getGroupWall(group.getId());
    }

    @Transactional
    public void deleteMessage(Long id) {
        messageDao.deleteMessage(id);
    }

    public List<Message> getDialogue(Account account, Account friend) {
        logger.info("get dialogue service");
        return messageDao.getDialogue(account.getId(), friend.getId());
    }

    public List<Account> getDialogues(Account account) {
        logger.info("get dialogues service");
        return messageDao.getInterlocutors(account.getId());
    }

    @Transactional
    public Long getDialogueId(Long account, Long friend) {
        logger.info("get dialogue id");
        Long dialogueId = messageDao.getDialogueId(account, friend);
        if (dialogueId != null) {
            logger.info("dialogue exist, id = {}", dialogueId);
            return dialogueId;
        } else {
            Dialogues dialogues = new Dialogues(account, friend);
            messageDao.insertDialogue(dialogues);
            logger.info("dialogue not exist {}", dialogues.getId());
            return dialogues.getId();
        }
    }
}
