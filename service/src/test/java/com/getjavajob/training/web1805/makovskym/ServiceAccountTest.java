package com.getjavajob.training.web1805.makovskym;

import com.getjavajob.training.web1805.makovskym.entities.Account;
import com.getjavajob.training.web1805.makovskym.entities.Friendships;
import com.getjavajob.training.web1805.makovskym.enums.AccountRoles;
import com.getjavajob.training.web1805.makovskym.enums.FriendsRelationships;
import com.getjavajob.training.web1805.makovskym.impl.AccountDaoImpl;
import com.getjavajob.training.web1805.makovskym.impl.PhoneRepository;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class ServiceAccountTest {
    private static List<Account> accounts;
    private static Account account;
    private final AccountDaoImpl accountDao = mock(AccountDaoImpl.class);
    private final PhoneRepository phoneDao = mock(PhoneRepository.class);
    private final AccountService serviceAccount = new AccountService(accountDao, phoneDao);

    @BeforeClass
    public static void createAccountListTest() {
        account = new Account("Dmitrov", "Petr", "fffreq", "112233r4455", AccountRoles.USER, new Date(2013 - 5 - 11));
        account.setId(1L);
        Account newAccount1 = new Account("Efimov", "Konstantin", "fffrew", "11221334455", AccountRoles.USER, new Date(2012 - 5 - 11));
        newAccount1.setId(2L);
        Account newAccount2 = new Account("Korolev", "Andru", "fffree", "11223344255", AccountRoles.USER, new Date(2011 - 4 - 11));
        newAccount2.setId(3L);
        Account newAccount3 = new Account("Igorev", "Konstantin", "fffrer", "112233445q5", AccountRoles.USER, new Date(2018 - 5 - 11));
        newAccount3.setId(4L);
        accounts = new ArrayList<>();
        accounts.add(newAccount1);
        accounts.add(newAccount2);
        accounts.add(newAccount3);
    }

    @Test
    public void getBySubstring() throws SQLException {
        when(accountDao.getBySubstring("o")).thenReturn(accounts);
        assertEquals(3, serviceAccount.getBySubstring("o").size());
    }

    @Test
    public void getAccountByLogin() throws SQLException {
        String login = "fffreq";
        when(accountDao.getByLogin(login)).thenReturn(account);
        assertEquals(account, serviceAccount.getAccountByLogin(login));
    }

    @Test
    public void getAccount() throws SQLException {
        when(accountDao.get(1L)).thenReturn(account);
        assertEquals(account, serviceAccount.getAccount(1L));
    }

    @Test
    public void createAccountTest() throws SQLException {
        serviceAccount.createAccount(account);
        verify(accountDao, atLeastOnce()).insertAccount(account);
    }

    @Test
    public void addFriend() throws SQLException {
        serviceAccount.addFriend(account, accounts.get(0));
        Friendships friendships = new Friendships();
        friendships.setAccountId(2L);
        friendships.setFriendId(1L);
        friendships.setRelationship(FriendsRelationships.FRIEND);
        Friendships secondFriendships = new Friendships();
        secondFriendships.setAccountId(1L);
        secondFriendships.setFriendId(2L);
        secondFriendships.setRelationship(FriendsRelationships.FRIEND);
        verify(accountDao, atLeastOnce()).insertFriend(friendships);
    }

    @Test
    public void updateAccount() throws SQLException {
        serviceAccount.updateAccount(account);
        verify(accountDao, atLeastOnce()).updateAccount(account);
    }

    @Test
    public void updateRole() throws SQLException {
        serviceAccount.updateRole(account, AccountRoles.ADMIN);
        verify(accountDao, atLeastOnce()).updateAccount(account);
    }

    @Test
    public void updatePassword() throws SQLException {
        serviceAccount.updatePassword(account, "123");
        verify(accountDao, atLeastOnce()).updateAccount(account);
    }

    @Test
    public void deleteAccount() throws SQLException {
        serviceAccount.deleteAccount(account);
        verify(accountDao, atLeastOnce()).deleteAccount(account);
    }

}
