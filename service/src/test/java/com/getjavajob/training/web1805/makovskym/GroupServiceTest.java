package com.getjavajob.training.web1805.makovskym;

import com.getjavajob.training.web1805.makovskym.entities.Account;
import com.getjavajob.training.web1805.makovskym.entities.Group;
import com.getjavajob.training.web1805.makovskym.entities.Memberships;
import com.getjavajob.training.web1805.makovskym.enums.AccountRoles;
import com.getjavajob.training.web1805.makovskym.enums.RoleInGroup;
import com.getjavajob.training.web1805.makovskym.impl.GroupRepository;
import com.getjavajob.training.web1805.makovskym.impl.MembershipsDaoImpl;
import com.getjavajob.training.web1805.makovskym.keys.MembershipsKey;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class GroupServiceTest {
    private static List<Group> groups;
    private static Account account;
    private static Group group;
    private final GroupRepository groupDao = mock(GroupRepository.class);
    private final MembershipsDaoImpl membershipsDao = mock(MembershipsDaoImpl.class);
    private final GroupService groupService = new GroupService(groupDao, membershipsDao);

    @BeforeClass
    public static void createAccountListTest() {
        account = new Account("Dmitrov", "Petr", "fffre", "1122334455", AccountRoles.USER, new Date(2018 - 5 - 11));
        account.setId(1L);
        group = new Group("devils", "Devil in your head", new Date(1981 - 5 - 11), account);
        Group group1 = group = new Group("devils", "Devil in your head", new Date(1971 - 2 - 11), account);
        group1.setId(2L);
        Group group2 = group = new Group("dark", "your head", new Date(1981 - 3 - 11), account);
        group2.setId(3L);
        Group group3 = group = new Group("white", "Devil", new Date(1991 - 5 - 11), account);
        group3.setId(4L);
        groups = new ArrayList<>();
        groups.add(group1);
        groups.add(group2);
        groups.add(group3);
    }

    @Test
    public void createGroup() {
        groupService.createGroup(group);
        verify(groupDao, atLeastOnce()).save(group);
    }

    @Test
    public void addFollower() {
        groupService.addFollower(group, account);
        Memberships memberships = new Memberships();
        memberships.setAccountId(account.getId());
        memberships.setGroupId(group.getId());
        memberships.setRole(RoleInGroup.FOLLOWER);
        verify(membershipsDao, atLeastOnce()).insertFollower(memberships);
    }

    @Test
    public void addRequest() throws SQLException {
        groupService.addRequest(group, account);
        Memberships memberships = new Memberships();
        memberships.setAccountId(account.getId());
        memberships.setGroupId(group.getId());
        memberships.setRole(RoleInGroup.REQUEST);
        verify(membershipsDao, atLeastOnce()).insertFollower(memberships);
    }

    @Test
    public void updateGroup() throws SQLException {
        groupService.updateGroup(group);
        verify(groupDao, atLeastOnce()).save(group);
    }

    @Test
    public void updateRole() throws SQLException {
        groupService.updateRole(group, account, RoleInGroup.ADMIN);
        Memberships memberships = new Memberships();
        memberships.setGroupId(group.getId());
        memberships.setAccountId(account.getId());
        memberships.setRole(RoleInGroup.ADMIN);
        verify(membershipsDao, atLeastOnce()).updateRole(memberships);
    }

    @Test
    public void getGroupsBuSubstr() throws SQLException {
        when(groupDao.findAllByNameOfGroup("o")).thenReturn(groups);
        assertEquals(3, groupService.getGroupsBuSubstr("o").size());
    }

    @Test
    public void getRole() throws SQLException {
        groupService.getRole(group, account);
        verify(membershipsDao, atLeastOnce()).getRole(group.getId(), account.getId());
    }

    @Test
    public void deleteGroup() throws SQLException {
        groupService.deleteGroup(group);
        verify(groupDao, atLeastOnce()).delete(group);
    }

    @Test
    public void deleteFollower() throws SQLException {
        groupService.deleteFollower(group, account);
        MembershipsKey key = new MembershipsKey(group.getId(), account.getId());
        verify(membershipsDao, atLeastOnce()).deleteFollower(key);
    }


}
