package com.getjavajob.training.web1805.makovskym;

import com.getjavajob.training.web1805.makovskym.entities.Account;
import com.getjavajob.training.web1805.makovskym.entities.Group;
import com.getjavajob.training.web1805.makovskym.entities.Message;
import com.getjavajob.training.web1805.makovskym.enums.AccountRoles;
import com.getjavajob.training.web1805.makovskym.impl.MessageDaoImpl;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class MessageServiceTest {
    private static List<Message> messages;
    private static Account account;
    private static Account account2;
    private static Group group;
    private static Message message;
    private final MessageDaoImpl messageDao = mock(MessageDaoImpl.class);
    private final MessageService messageService = new MessageService(messageDao);

    @BeforeClass
    public static void createAccountListTest() {
        account = new Account("Dmitrov", "Petr", "fffre", "1122334455", AccountRoles.USER, new Date(2018 - 5 - 11));
        account.setId(1L);
        account2 = new Account("Efimov", "Konstantin", "fffreq", "112233q4455", AccountRoles.USER, new Date(2018 - 5 - 11));
        account2.setId(2L);
        group = new Group("Black", "blaks", new Date(1981 - 5 - 11), account);
        group.setId(1L);
        Message message1 = new Message(account, new Timestamp(new java.util.Date().getTime()), "Message 1", 1, 2);
        message1.setId(1L);
        Message message2 = new Message(account, new Timestamp(new java.util.Date().getTime()), "Message 2", 1, 2);
        message2.setId(2L);
        Message message3 = new Message(account, new Timestamp(new java.util.Date().getTime()), "Message 3", 1, 2);
        message3.setId(3L);
        messages = new ArrayList<>();
        messages.add(message1);
        messages.add(message2);
        messages.add(message3);
        message = new Message(account, new Timestamp(new java.util.Date().getTime()), "Message", 1, 2);
        message.setId(4L);
    }

    @Test
    public void getAccountWall() throws SQLException {
        when(messageDao.getAccountWall(1L)).thenReturn(messages);
        assertEquals(messages, messageService.getAccountWall(account));
    }

    @Test
    public void getGroupWall() throws SQLException {
        when(messageDao.getGroupWall(1L)).thenReturn(messages);
        assertEquals(messages, messageService.getGroupWall(group));
    }

    @Test
    public void getDialogue() throws SQLException {
        when(messageDao.getDialogue(1L, 2L)).thenReturn(messages);
        assertEquals(messages, messageService.getDialogue(account, account2));
    }

    @Test
    public void createPrivateMessage() throws SQLException {
        messageService.createPrivateMessage(message);
        verify(messageDao, atLeastOnce()).insert(message);
    }

    @Test
    public void createAccountWallMessage() throws SQLException {
        messageService.createAccountWallMessage(message);
        verify(messageDao, atLeastOnce()).insert(message);
    }

    @Test
    public void createGroupWallMessage() throws SQLException {
        messageService.createGroupWallMessage(message);
        verify(messageDao, atLeastOnce()).insert(message);
    }
}
