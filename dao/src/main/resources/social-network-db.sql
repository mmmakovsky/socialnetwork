CREATE TABLE accounts_tb (
  account_id        BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  second_name       VARCHAR(15)  NOT NULL,
  first_name        VARCHAR(15)  NOT NULL,
  middle_name       VARCHAR(15)  NOT NULL,
  birthday          DATE,
  home_address      VARCHAR(255),
  work_address      VARCHAR(255),
  email             VARCHAR(255) NOT NULL,
  icq               INTEGER(9),
  skype             VARCHAR(32),
  more_info         VARCHAR(775),
  registration_date DATE         NOT NULL,
  login             VARCHAR(16)  NOT NULL,
  password          VARCHAR(32)  NOT NULL,
  role              enum("ADMUN", "USER", "MODERATOR"),
  photo_id          BIGINT UNSIGNED,
  FOREIGN KEY (photo_id) REFERENCES photos_tb (photo_id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
  PRIMARY KEY (account_id)
);

CREATE TABLE groups_tb (
  group_id     BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  name         VARCHAR(15) NOT NULL,
  created_date DATE        NOT NULL,
  creator_id   BIGINT UNSIGNED,
  description  VARCHAR(775),
  photo_id     BIGINT UNSIGNED,
  FOREIGN KEY (photo_id) REFERENCES photos_tb (photo_id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
  FOREIGN KEY (creator_id) REFERENCES ACCOUNTS_tb (account_id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
  PRIMARY KEY (group_id)
);

CREATE TABLE friendships_tb (
  account_id BIGINT UNSIGNED NOT NULL,
  FRIEND_id  BIGINT UNSIGNED NOT NULL,
  conditions enum("FRIEND", "FOLLOWER"),,
  FOREIGN KEY (account_id) REFERENCES accounts_tb (account_id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
  FOREIGN KEY (FRIEND_id) REFERENCES accounts_tb (account_id)
  ON DELETE CASCADE
  ON UPDATE CASCADE
);

CREATE TABLE followers_tb (
  group_id    BIGINT UNSIGNED NOT NULL,
  FOLLOWER_id BIGINT UNSIGNED NOT NULL,
  role        enum("ADMUN", "FOLLOWER", "REQUEST"),,
  FOREIGN KEY (group_id) REFERENCES groups_tb (group_id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
  FOREIGN KEY (FOLLOWER_id) REFERENCES accounts_tb (account_id)
  ON DELETE CASCADE
  ON UPDATE CASCADE
);

CREATE TABLE phones_tb (
  account_id BIGINT UNSIGNED NOT NULL,
  phone      CHAR(12) NOT NULL,
  type       enum("HOME", "WORK"),
  FOREIGN KEY (account_id) REFERENCES accounts_tb (account_id)
  ON DELETE CASCADE
  ON UPDATE CASCADE
);

CREATE TABLE messages_tb (
  msg_id         BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  author_id      BIGINT UNSIGNED NOT NULL,
  destination_id BIGINT UNSIGNED NOT NULL,
  type           enum("PRIVATE", "PUBLIC", "GROUP"),
  sent_date      DATETIME NOT NULL,
  photo_id       BIGINT UNSIGNED,
  msg            TEXT,
  FOREIGN KEY (author_id) REFERENCES accounts_tb (account_id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
  FOREIGN KEY (photo_id) REFERENCES photos_tb (photo_id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
  PRIMARY KEY (msg_id)
);

CREATE TABLE photos_tb (
  photo_id  BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  photo     BLOB NOT NULL,
  author_id BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (photo_id)
);

CREATE TABLE dialogues_tb (
  dialogue_idBIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  account_id BIGINT UNSIGNED NOT NULL,
  friend_id  BIGINT UNSIGNED NOT NULL,
  FOREIGN KEY (account_id) REFERENCES accounts_tb (account_id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
  FOREIGN KEY (friend_id) REFERENCES accounts_tb (account_id)
  ON DELETE CASCADE
  ON UPDATE CASCADE
);