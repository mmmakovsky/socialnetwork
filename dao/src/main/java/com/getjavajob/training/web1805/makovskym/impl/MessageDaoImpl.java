package com.getjavajob.training.web1805.makovskym.impl;


import com.getjavajob.training.web1805.makovskym.MessageDao;
import com.getjavajob.training.web1805.makovskym.entities.Account;
import com.getjavajob.training.web1805.makovskym.entities.Dialogues;
import com.getjavajob.training.web1805.makovskym.entities.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import java.util.List;

@Repository
public class MessageDaoImpl implements MessageDao {
    private static final String SELECT_DIALOGUES = "FROM Account WHERE id in  " +
            "(SELECT author from Message where type='PRIVATE' and destinationId =:addressee) " +
            "or id in (SELECT destinationId from Message where type='PRIVATE' and author =:addressee)";
    private static final String SELECT_DIALOGUE = "FROM Message where ((author_id = :author and destination_id= :addressee) " +
            "or (author_id= :addressee and destination_id = :author )) and type='PRIVATE' order by sent_date";
    private static final String SELECT_ACCOUNT_WALL_POST = "FROM Message " +
            "where destination_id = :dest and type='PUBLIC' order by sent_date desc ";
    private static final String SELECT_GROUP_WALL_POST = "FROM Message " +
            "where destination_id = :dest and type='GROUP' order by sent_date desc ";
    private static final String SELECT_DIALOGUE_ID = "from Dialogues where account_id= :account and friend_id= :friend " +
            "or account_id= :friend and  friend_id= :account";
    private static final Logger logger = LoggerFactory.getLogger(MessageDaoImpl.class);

    @PersistenceContext(type = PersistenceContextType.EXTENDED)
    private EntityManager entityManager;

    @Override
    public List<Account> getInterlocutors(Long accountID) {
        logger.info("get interlocutors ");
        return entityManager.createQuery(SELECT_DIALOGUES, Account.class)
                .setParameter("addressee", accountID).getResultList();
    }

    @Override
    public List<Message> getDialogue(Long accountID, Long destinationId) {
        logger.info("get dialogue");
        return entityManager.createQuery(SELECT_DIALOGUE, Message.class)
                .setParameter("addressee", destinationId).setParameter("author", accountID).getResultList();
    }

    @Override
    public Long getDialogueId(Long accountID, Long destinationId) {
        Long dialogues = null;
        try {
            dialogues = entityManager.createQuery(SELECT_DIALOGUE_ID, Dialogues.class)
                    .setParameter("account", accountID)
                    .setParameter("friend", destinationId)
                    .getSingleResult().getId();
        } catch (NoResultException e) {
            logger.info("no dialogues");
        }
        return dialogues;
    }

    @Override
    public void insertDialogue(Dialogues dialogues) {
        entityManager.persist(dialogues);
        entityManager.flush();
    }

    @Override
    public List<Message> getAccountWall(Long accountID) {
        logger.info("get account wall");
        return entityManager.createQuery(SELECT_ACCOUNT_WALL_POST, Message.class)
                .setParameter("dest", accountID).getResultList();
    }

    @Override
    public List<Message> getGroupWall(Long groupID) {
        logger.info("get group wall");
        return entityManager.createQuery(SELECT_GROUP_WALL_POST, Message.class)
                .setParameter("dest", groupID).getResultList();
    }

    @Override
    public void insert(Message msg) {
        logger.info("Insert start");
        entityManager.persist(msg);
        entityManager.flush();
        logger.info("Insert end flush");
    }

    @Override
    public void deleteMessage(Long id) {
        logger.info("message remove");
        entityManager.remove(entityManager.find(Message.class, id));
        logger.info("end remove message");
    }
}
