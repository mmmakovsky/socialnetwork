package com.getjavajob.training.web1805.makovskym;

import com.getjavajob.training.web1805.makovskym.entities.Account;
import com.getjavajob.training.web1805.makovskym.entities.Friendships;
import com.getjavajob.training.web1805.makovskym.entities.Photo;
import com.getjavajob.training.web1805.makovskym.keys.FriendshipsKey;

import java.util.List;

public interface AccountDao {

    Account get(Long id);

    List<Account> getBySubstring(String substring);

    Photo getPhoto(Long photoId);

    List<Photo> getAccountPhotos(Long account);

    void insertPhoto(Photo photo);

    Account getByLogin(String login);

    void insertAccount(Account account);

    void insertFriend(Friendships friendships);

    void updateAccount(Account account);

    void updateFriendship(FriendshipsKey friendshipsKey);

    void deleteAccount(Account account);

    void deleteFriend(FriendshipsKey key);

    void deletePhoto(Long id);

}
