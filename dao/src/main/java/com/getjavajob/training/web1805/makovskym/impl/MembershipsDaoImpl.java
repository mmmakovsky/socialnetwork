package com.getjavajob.training.web1805.makovskym.impl;

import com.getjavajob.training.web1805.makovskym.entities.Memberships;
import com.getjavajob.training.web1805.makovskym.enums.RoleInGroup;
import com.getjavajob.training.web1805.makovskym.keys.MembershipsKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

@Repository
public class MembershipsDaoImpl {
    private static final Logger logger = LoggerFactory.getLogger(AccountDaoImpl.class);
    @PersistenceContext(type = PersistenceContextType.EXTENDED)
    private EntityManager entityManager;

    public RoleInGroup getRole(Long group, Long accountId) {
        logger.info("get role");
        MembershipsKey key = new MembershipsKey(group, accountId);
        Memberships memberships = entityManager.find(Memberships.class, key);
        if (memberships != null) {
            return memberships.getRole();
        } else {
            return null;
        }
    }

    public void insertFollower(Memberships memberships) {
        logger.info("insert follower");
        entityManager.persist(memberships);
        entityManager.flush();
    }

    public void updateRole(Memberships memberships) {
        logger.info("update role");
        entityManager.merge(memberships);
    }

    public void deleteFollower(MembershipsKey memberships) {
        logger.info("delete follower");
        entityManager.remove(entityManager.find(Memberships.class, memberships));
        entityManager.flush();
    }
}
