package com.getjavajob.training.web1805.makovskym.impl;

import com.getjavajob.training.web1805.makovskym.entities.Group;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupRepository extends CrudRepository<Group, Long> {

    @Query(value = "FROM Group where name like concat('%',:substr,'%')")
    List<Group> findAllByNameOfGroup(@Param("substr") String name);

}
