package com.getjavajob.training.web1805.makovskym.impl;

import com.getjavajob.training.web1805.makovskym.AccountDao;
import com.getjavajob.training.web1805.makovskym.entities.Account;
import com.getjavajob.training.web1805.makovskym.entities.Friendships;
import com.getjavajob.training.web1805.makovskym.entities.Photo;
import com.getjavajob.training.web1805.makovskym.enums.FriendsRelationships;
import com.getjavajob.training.web1805.makovskym.keys.FriendshipsKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;


@Repository
public class AccountDaoImpl implements AccountDao {
    private static final String SELECT_ALL = "FROM Account";
    private static final String SELECT_BY_SUBSTR =
            "FROM Account where first_name like concat('%',:substring,'%') or second_name like concat('%',:substring,'%')";
    private static final String SELECT_BY_LOGIN = SELECT_ALL + " WHERE login = :login";
    private static final String SELECT_ACCOUNT_PHOTOS = "from Photo where author_id = :author";
    private static final Logger logger = LoggerFactory.getLogger(AccountDaoImpl.class);

    @PersistenceContext
    private EntityManager em;

    @Override
    public Account get(Long id) {
        logger.info("get account");
        return em.find(Account.class, id);
    }

    @Override
    public List<Account> getBySubstring(String substring) {
        logger.info("get account by substring");
        return em.createQuery(SELECT_BY_SUBSTR, Account.class).setParameter("substring", substring).getResultList();
    }

    @Override
    public Account getByLogin(String login) {
        logger.info("get account by login");
        return em.createQuery(SELECT_BY_LOGIN, Account.class).setParameter("login", login)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void insertAccount(Account account) {
        logger.info("insert account");
        em.persist(account);
        em.flush();
    }

    @Override
    public void updateFriendship(FriendshipsKey friendshipsKey) {
        logger.info("update friendship");
        Friendships friendships = em.find(Friendships.class, friendshipsKey);
        friendships.setRelationship(FriendsRelationships.FRIEND);
        em.merge(friendships);
        em.flush();
    }

    @Override
    public void insertFriend(Friendships friendships) {
        logger.info("insert frendships");
        em.persist(friendships);
        em.flush();
    }

    @Override
    public void updateAccount(Account account) {
        logger.info("Start UpdateAccountDAO");
        em.merge(account);
        em.flush();
    }

    @Override
    public void deleteAccount(Account account) {
        logger.info("delete account");
        em.remove(em.merge(account));
    }

    @Override
    public void deleteFriend(FriendshipsKey key) {
        logger.info("delete friend");
        em.remove(em.find(Friendships.class, key));
        em.flush();
    }

    @Override
    public Photo getPhoto(Long photoId) {
        logger.info("get photo by id {}", photoId);
        return em.find(Photo.class, photoId);
    }

    @Override
    public void deletePhoto(Long id) {
        logger.info("delete photo");
        em.remove(em.find(Photo.class, id));
    }

    @Override
    public List<Photo> getAccountPhotos(Long account) {
        logger.info("get account photos");
        return em.createQuery(SELECT_ACCOUNT_PHOTOS, Photo.class).setParameter("author", account).getResultList();
    }

    @Override
    public void insertPhoto(Photo photo) {
        logger.info("insert photo ");
        em.persist(photo);
    }
}
