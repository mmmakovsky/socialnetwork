package com.getjavajob.training.web1805.makovskym;

import com.getjavajob.training.web1805.makovskym.entities.Account;
import com.getjavajob.training.web1805.makovskym.entities.Dialogues;
import com.getjavajob.training.web1805.makovskym.entities.Message;

import java.util.List;

public interface MessageDao {

    List<Account> getInterlocutors(Long accountID);

    List<Message> getDialogue(Long accountID, Long destinationId);

    Long getDialogueId(Long accountID, Long destinationId);

    void insertDialogue(Dialogues dialogues);

    List<Message> getAccountWall(Long accountID);

    List<Message> getGroupWall(Long groupID);

    void insert(Message msg);

    void deleteMessage(Long id);

}
