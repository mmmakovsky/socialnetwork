DROP TABLE IF EXISTS messages_tb, phones_tb, FOLLOWERs_tb, groups_tb, FRIENDships_tb, accounts_tb, photos_tb;


CREATE TABLE photos_tb (
  photo_id  BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  photo     CHAR(10) NOT NULL,
  author_id BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (photo_id)
);

CREATE TABLE accounts_tb (
  account_id        BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  second_name       VARCHAR(15)  NOT NULL,
  first_name        VARCHAR(15)  NOT NULL,
  middle_name       VARCHAR(15),
  birthday          DATE,
  HOME_address      VARCHAR(255),
  WORK_address      VARCHAR(255),
  email             VARCHAR(255),
  icq               INTEGER(9),
  skype             VARCHAR(32),
  more_info         VARCHAR(775),
  registration_date DATE         NOT NULL,
  login             VARCHAR(16)  NOT NULL,
  password          VARCHAR(32)  NOT NULL,
  role              VARCHAR(32),
  photo_id          BIGINT UNSIGNED,
  FOREIGN KEY (photo_id) REFERENCES photos_tb (photo_id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
  PRIMARY KEY (account_id)
);

CREATE TABLE groups_tb (
  group_id     BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  name         VARCHAR(15) NOT NULL,
  created_date DATE        NOT NULL,
  creator_id   BIGINT UNSIGNED,
  description  VARCHAR(775),
  photo_id     BIGINT UNSIGNED,
  FOREIGN KEY (photo_id) REFERENCES photos_tb (photo_id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
  FOREIGN KEY (creator_id) REFERENCES ACCOUNTS_tb (account_id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
  PRIMARY KEY (group_id)
);

CREATE TABLE FRIENDships_tb (
  account_id BIGINT UNSIGNED NOT NULL,
  FRIEND_id  BIGINT UNSIGNED NOT NULL,
  conditions VARCHAR(32),
  FOREIGN KEY (account_id) REFERENCES accounts_tb (account_id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
  FOREIGN KEY (FRIEND_id) REFERENCES accounts_tb (account_id)
  ON DELETE CASCADE
  ON UPDATE CASCADE
);

CREATE TABLE FOLLOWERs_tb (
  group_id    BIGINT UNSIGNED NOT NULL,
  FOLLOWER_id BIGINT UNSIGNED NOT NULL,
  role        VARCHAR(32),
  FOREIGN KEY (group_id) REFERENCES groups_tb (group_id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
  FOREIGN KEY (FOLLOWER_id) REFERENCES accounts_tb (account_id)
  ON DELETE CASCADE
  ON UPDATE CASCADE
);

CREATE TABLE phones_tb (
  id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  account_id BIGINT UNSIGNED NOT NULL,
  phone      CHAR(18) NOT NULL,
  type       VARCHAR(32),
  FOREIGN KEY (account_id) REFERENCES accounts_tb (account_id)
  ON DELETE CASCADE
  ON UPDATE CASCADE
);

CREATE TABLE messages_tb (
  msg_id         BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  author_id      BIGINT UNSIGNED NOT NULL,
  destination_id BIGINT UNSIGNED NOT NULL,
  type           VARCHAR(32),
  sent_date      DATETIME NOT NULL,
  photo_id       BIGINT UNSIGNED,
  msg            TEXT,
  FOREIGN KEY (author_id) REFERENCES accounts_tb (account_id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
  FOREIGN KEY (photo_id) REFERENCES photos_tb (photo_id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
  PRIMARY KEY (msg_id)
);

INSERT INTO photos_tb (photo, author_id) VALUES ('d', 1);
INSERT INTO photos_tb (photo, author_id) VALUES ('d', 1);
INSERT INTO photos_tb (photo, author_id) VALUES ('d', 1);
INSERT INTO photos_tb (photo, author_id) VALUES ('d', 1);

INSERT INTO accounts_tb (second_name, first_name, middle_name, birthday, HOME_address, WORK_address, email, icq, skype, more_info, registration_date, login, password, role, photo_id)
VALUES
  ('Ivanov', 'Ivan', 'Ivanovich', '1991-02-12', 'ul.Serova, 16', 'ul.Dovatorcev, 15', 'wewq.rt@yandex.ru', 123123123,
             'wes.wwe', 'dfsdfsd;flssdf sdfsdf d  sdfs', '2018-07-18', 'wewq.rt@yande', '12343234', 'USER', NULL);
INSERT INTO accounts_tb (second_name, first_name, middle_name, birthday, HOME_address, WORK_address, email, icq, skype, more_info, registration_date, login, password, role, photo_id)
VALUES ('Petrov', 'Vasiliy', 'Ivanovich', '1993-12-12', 'ul.Serafova, 13', 'ul.Dovatorcev, 15', 'aws.rt@yandex.ru',
                  123123123, 'wes.wwe', 'dfsdfsd;flssdf sdfsdf d  sdfs', '2018-07-18', 'aws.rt', '11234324', 'USER',
        NULL);
INSERT INTO accounts_tb (second_name, first_name, middle_name, birthday, HOME_address, WORK_address, email, icq, skype, more_info, registration_date, login, password, role, photo_id)
VALUES ('Avdeenco', 'Andrey', 'Ivanovich', '1991-02-12', 'ul.Serova, 16', 'ul.Dovatorcev, 15', 'wewq.rt@yandex.ru',
                    123123123, 'wes.wwe', 'dfsdfsd;flssdf sdfsdf d  sdfs', '2018-07-18', 'wewq', '12343224', 'USER',
        NULL);
INSERT INTO accounts_tb (second_name, first_name, middle_name, birthday, HOME_address, WORK_address, email, icq, skype, more_info, registration_date, login, password, role, photo_id)
VALUES
  ('Safin', 'Ilia', 'Ivanovich', '1991-02-12', 'ul.Serova, 16', 'ul.Dovatorcev, 15', 'wewq.rt@yandex.ru', 123123123,
            'wes.wwe', 'dfsdfsd;flssdf sdfsdf d  sdfs', '2018-07-18', 'wewq.r', '123432344', 'USER', NULL);
INSERT INTO accounts_tb (second_name, first_name, middle_name, birthday, HOME_address, WORK_address, email, icq, skype, more_info, registration_date, login, password, role, photo_id)
VALUES
  ('Kurdov', 'Seb', 'Ivanovich', '1991-02-12', 'ul.Serova, 16', 'ul.Dovatorcev, 15', 'wewq.rt@yandex.ru', 123123123,
             'wes.wwe', 'dfsdfsd;flssdf sdfsdf d  sdfs', '2018-07-18', 'andex.ru', '1234324', 'USER', NULL);

INSERT INTO groups_tb (name, created_date, creator_id, description, photo_id)
VALUES ('dark', '2018-01-21', 1, 'dark in our mind', NULL);
INSERT INTO groups_tb (name, created_date, creator_id, description, photo_id)
VALUES ('white', '2018-01-21', 2, 'dark in our mind', NULL);
INSERT INTO groups_tb (name, created_date, creator_id, description, photo_id)
VALUES ('grey', '2018-01-21', 3, 'dark in our mind', NULL);
INSERT INTO groups_tb (name, created_date, creator_id, description, photo_id)
VALUES ('blind side', '2018-01-21', 4, 'dark in our mind', NULL);
INSERT INTO groups_tb (name, created_date, creator_id, description, photo_id)
VALUES ('black', '2018-01-21', 2, 'dark in our mind', NULL);


INSERT INTO FRIENDships_tb (account_id, FRIEND_id, conditions) VALUES (1, 2, 'FRIEND');
INSERT INTO FRIENDships_tb (account_id, FRIEND_id, conditions) VALUES (1, 3, 'FRIEND');
INSERT INTO FRIENDships_tb (account_id, FRIEND_id, conditions) VALUES (1, 4, 'FOLLOWER');
INSERT INTO FRIENDships_tb (account_id, FRIEND_id, conditions) VALUES (1, 5, 'FOLLOWER');
INSERT INTO FRIENDships_tb (account_id, FRIEND_id, conditions) VALUES (2, 3, 'FRIEND');
INSERT INTO FRIENDships_tb (account_id, FRIEND_id, conditions) VALUES (2, 4, 'FOLLOWER');

INSERT INTO FOLLOWERs_tb (group_id, FOLLOWER_id, role) VALUES (1, 2, 'ADMIN');
INSERT INTO FOLLOWERs_tb (group_id, FOLLOWER_id, role) VALUES (1, 1, 'FOLLOWER');
INSERT INTO FOLLOWERs_tb (group_id, FOLLOWER_id, role) VALUES (2, 1, 'FOLLOWER');
INSERT INTO FOLLOWERs_tb (group_id, FOLLOWER_id, role) VALUES (2, 2, 'ADMIN');
INSERT INTO FOLLOWERs_tb (group_id, FOLLOWER_id, role) VALUES (3, 1, 'request');
INSERT INTO FOLLOWERs_tb (group_id, FOLLOWER_id, role) VALUES (3, 2, 'request');

INSERT INTO phones_tb (account_id, phone, type) VALUES (1, '+79232342341', 'HOME');
INSERT INTO phones_tb (account_id, phone, type) VALUES (1, '+79233442341', 'HOME');
INSERT INTO phones_tb (account_id, phone, type) VALUES (1, '+79235642341', 'WORK');
INSERT INTO phones_tb (account_id, phone, type) VALUES (1, '+79256542341', 'HOME');
INSERT INTO phones_tb (account_id, phone, type) VALUES (2, '+79232398341', 'HOME');
INSERT INTO phones_tb (account_id, phone, type) VALUES (3, '+79232367341', 'HOME');
INSERT INTO phones_tb (account_id, phone, type) VALUES (3, '+79232209341', 'WORK');
INSERT INTO phones_tb (account_id, phone, type) VALUES (4, '+79232300341', 'HOME');
INSERT INTO phones_tb (account_id, phone, type) VALUES (4, '+79232311141', 'WORK');
INSERT INTO phones_tb (account_id, phone, type) VALUES (4, '+79232355541', 'HOME');
INSERT INTO phones_tb (account_id, phone, type) VALUES (5, '+79237777741', 'HOME');


INSERT INTO messages_tb (author_id, destination_id, type, sent_date, photo_id, msg)
VALUES (1, 2, 'private', '2018-05-24', 1, 'hi');
INSERT INTO messages_tb (author_id, destination_id, type, sent_date, msg) VALUES (2, 1, 'PRIVATE', '2018-05-24', 'hi');
INSERT INTO messages_tb (author_id, destination_id, type, sent_date, msg) VALUES (1, 1, 'PUBLIC', '2018-05-24', 'hi1');
INSERT INTO messages_tb (author_id, destination_id, type, sent_date, msg) VALUES (1, 1, 'PUBLIC', '2018-05-24', 'h3i2');
INSERT INTO messages_tb (author_id, destination_id, type, sent_date, msg) VALUES (1, 1, 'GROUP', '2018-05-24', 'hi3');
INSERT INTO messages_tb (author_id, destination_id, type, sent_date, msg) VALUES (1, 1, 'GROUP', '2018-05-24', 'hi4');
