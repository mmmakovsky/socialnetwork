package com.getjavajob.training.web1805.makovskym.impl;

import com.getjavajob.training.web1805.makovskym.AccountDao;
import com.getjavajob.training.web1805.makovskym.DaoApplication;
import com.getjavajob.training.web1805.makovskym.MessageDao;
import com.getjavajob.training.web1805.makovskym.entities.Account;
import com.getjavajob.training.web1805.makovskym.entities.Message;
import com.getjavajob.training.web1805.makovskym.enums.MessageType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = DaoApplication.class)
@TestPropertySource(
        locations = "classpath:application-test.properties")
@AutoConfigureTestEntityManager
@Transactional
public class MessageDaoImplTest {
    @Autowired
    private MessageDao messageDao;
    @Autowired
    private AccountDao accountDao;

    @Test
    public void getInterlocutersTest() throws SQLException {
        assertEquals(1, messageDao.getInterlocutors(1L).size());
    }

    @Test
    public void getDialogueTest() throws SQLException {
        assertEquals(1, messageDao.getDialogue(1L, 2L).size());
    }

    @Test
    public void getAccountWallTest() {
        assertEquals(2, messageDao.getAccountWall(1L).size());
    }

    @Test
    public void getGroupWallTest() throws SQLException {
        assertEquals(2, messageDao.getGroupWall(1L).size());
    }

    @Test
    public void insertPrivateTest() throws SQLException {
        Account account = this.accountDao.get(1L);
        Message msg = new Message(account, new Timestamp(new java.util.Date().getTime()), "Hell", 1, 3);
        msg.setType(MessageType.PRIVATE);
        messageDao.insert(msg);
        List<Message> messages = messageDao.getDialogue(1L, 3L);
        List<String> ms = new ArrayList<>();
        for (Message m : messages) {
            ms.add(m.getMsg());
        }
        assertEquals(true, ms.contains("Hell"));
    }

    @Test(expected = Exception.class)
    public void insertPrivateExceptionTest() throws SQLException {
        Message msg = new Message(null, new Timestamp(new java.util.Date().getTime()), "hi", 1, 3);
        messageDao.insert(msg);
    }

    @Test(expected = Exception.class)
    public void insertToAccountWallExceptionTest() throws SQLException {
        Message msg = new Message(null, new Timestamp(new java.util.Date().getTime()), "Hi!", 1, 3);
        messageDao.insert(msg);
    }

    @Test(expected = Exception.class)
    public void insertToGroupWallExceptionTest() throws SQLException {
        Message msg = new Message(null, new Timestamp(new java.util.Date().getTime()), "hello, hello", 1, 3);
        messageDao.insert(msg);
    }

    @Test
    public void delete() throws SQLException {
        Account account = this.accountDao.get(3L);
        Message msg = new Message(account, new Timestamp(new java.util.Date().getTime()), "how are u", 1, 5);
        msg.setType(MessageType.PRIVATE);
        messageDao.insert(msg);
        messageDao.deleteMessage(msg.getId());
        List<String> ms = new ArrayList<>();
        List<Message> messages = messageDao.getDialogue(3L, 5L);
        for (Message m : messages) {
            ms.add(m.getMsg());
        }
        assertEquals(false, ms.contains("Hello"));
    }
}
