package com.getjavajob.training.web1805.makovskym.impl;

import com.getjavajob.training.web1805.makovskym.DaoApplication;
import com.getjavajob.training.web1805.makovskym.entities.Account;
import com.getjavajob.training.web1805.makovskym.entities.Friendships;
import com.getjavajob.training.web1805.makovskym.enums.AccountRoles;
import com.getjavajob.training.web1805.makovskym.enums.FriendsRelationships;
import com.getjavajob.training.web1805.makovskym.keys.FriendshipsKey;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNull;


@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@SpringBootTest(classes = DaoApplication.class)
@TestPropertySource(
        locations = "classpath:application-test.properties")
public class AccountDaoImplTest {
    @Autowired
    private AccountDaoImpl accountDao;

    @Test
    public void getTest() {
        Account account = accountDao.get(1L);
        assertEquals("Ivan", account.getFirstName());
    }

    @Test
    public void getByLoginTest() {
        Account account = accountDao.getByLogin("wewq.rt@yande");
        assertEquals("Ivan", account.getFirstName());
    }



    @Test
    public void getByLoginTest1() {
        Account account = accountDao.getByLogin("wewq");
        assertEquals("Andrey", account.getFirstName());
    }

    @Test
    public void getBySubstringTest() {
        List<Account> accounts = accountDao.getBySubstring("ov");
        assertEquals(3, accounts.size());
    }

    @Test
    public void insertTest() {
        Account newAccount = new Account("Pavlov", "Sergey",
                "fffres", "11223344556", AccountRoles.USER, new Date(2018 - 5 - 11));
        accountDao.insertAccount(newAccount);
        assertEquals("Sergey", accountDao.get(newAccount.getId()).getFirstName());
    }

    @Test(expected = Exception.class)
    public void insertExceptionTest() {
        Account newAccount = new Account("Korolev", null,
                "fffre", "1122334455", AccountRoles.USER, new Date(2018 - 5 - 11));
        accountDao.insertAccount(newAccount);
    }

    @Test
    public void insertFriendTest() {
        Friendships friendships = new Friendships();
        friendships.setFriendId(4L);
        friendships.setAccountId(5L);
        friendships.setRelationship(FriendsRelationships.FRIEND);
        accountDao.insertFriend(friendships);
        Account account = accountDao.get(5L);
        Account friend = accountDao.get(4L);
        assertEquals(true, account.getFriends().contains(friend));
    }

    @Test(expected = Exception.class)
    public void insertFriendExceptionTest() {
        Friendships friendships = new Friendships();
        friendships.setFriendId(null);
        friendships.setAccountId(2L);
        friendships.setRelationship(FriendsRelationships.FRIEND);
        accountDao.insertFriend(friendships);
    }

    @Test
    public void insertFollowerTest() {
        Friendships friendships = new Friendships();
        friendships.setFriendId(3L);
        friendships.setAccountId(5L);
        friendships.setRelationship(FriendsRelationships.FOLLOWER);
        accountDao.insertFriend(friendships);
        Account account = accountDao.get(5L);
        Account friend = accountDao.get(3L);
        assertEquals(true, account.getFollowers().contains(friend));
    }

    @Test(expected = Exception.class)
    public void insertFollowerExceptionTest() {
        Friendships friendships = new Friendships();
        friendships.setFriendId(null);
        friendships.setAccountId(2L);
        friendships.setRelationship(FriendsRelationships.FOLLOWER);
        accountDao.insertFriend(friendships);
    }

    @Test
    public void updateTest() {
        Account account = accountDao.get(2L);
        account.setFirstName("Stanislav");
        accountDao.updateAccount(account);
        assertEquals("Stanislav", accountDao.get(2L).getFirstName());
    }

    @Test
    public void deleteTest() {
        Account newAccount = new Account("Korol", "Konst", "fsffre", "1122e334455", AccountRoles.USER, new Date(2018 - 6 - 11));
        accountDao.insertAccount(newAccount);
        accountDao.deleteAccount(newAccount);
        assertNull(accountDao.get(newAccount.getId()));
    }

    @Test
    public void deleteFriendTest() {
        Friendships friendships = new Friendships();
        friendships.setFriendId(2L);
        friendships.setAccountId(5L);
        friendships.setRelationship(FriendsRelationships.FRIEND);
        accountDao.insertFriend(friendships);
        accountDao.deleteFriend(new FriendshipsKey(5L, 2L));
        Account account = accountDao.get(5L);
        Account friend = accountDao.get(2L);
        assertEquals(false, account.getFriends().contains(friend));
    }
}
