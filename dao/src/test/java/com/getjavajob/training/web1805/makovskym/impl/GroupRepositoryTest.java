package com.getjavajob.training.web1805.makovskym.impl;

import com.getjavajob.training.web1805.makovskym.AccountDao;
import com.getjavajob.training.web1805.makovskym.DaoApplication;
import com.getjavajob.training.web1805.makovskym.entities.Account;
import com.getjavajob.training.web1805.makovskym.entities.Group;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = DaoApplication.class)
@TestPropertySource(
        locations = "classpath:application-test.properties")
@Transactional
public class GroupRepositoryTest {
    @Autowired
    private GroupRepository dao;
    @Autowired
    private AccountDao accountDao;

    @Test
    public void getTest() {
        Group group = dao.findById(1L).orElse(new Group());
        assertEquals("dark", group.getNameOfGroup());
    }

    @Test
    public void getBySubstrTest() {
        List<Group> groups = dao.findAllByNameOfGroup("e");
        assertEquals(3, groups.size());
    }

    @Test
    public void insertTest() {
        Account creator = accountDao.get(1L);
        String groupName = "devils";
        Group newGroup = new Group(groupName, "Devil in our head", new Date(1981 - 5 - 11), creator);
        dao.save(newGroup);
        Long id = newGroup.getId();
        assertEquals(groupName, dao.findById(id).orElse(new Group()).getNameOfGroup());
    }

    @Test(expected = Exception.class)
    public void insertExceptionTest() {
        Group newGroup = new Group("deviles", "Devil in your head", null, null);
        dao.save(newGroup);
    }

    @Test
    public void updateTest() {
        Group group = dao.findById(2L).orElse(new Group());
        group.setNameOfGroup("NewName");
        dao.save(group);
        assertEquals("NewName", dao.findById(2L).orElse(new Group()).getNameOfGroup());
    }

    @Test
    public void deleteTest() {
        dao.delete(dao.findById(5L).orElse(new Group()));
        assertNull(dao.findById(5L).orElse(null));
    }
}
