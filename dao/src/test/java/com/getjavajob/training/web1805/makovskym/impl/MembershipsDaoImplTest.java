package com.getjavajob.training.web1805.makovskym.impl;

import com.getjavajob.training.web1805.makovskym.DaoApplication;
import com.getjavajob.training.web1805.makovskym.entities.Account;
import com.getjavajob.training.web1805.makovskym.entities.Group;
import com.getjavajob.training.web1805.makovskym.entities.Memberships;
import com.getjavajob.training.web1805.makovskym.enums.RoleInGroup;
import com.getjavajob.training.web1805.makovskym.keys.MembershipsKey;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = DaoApplication.class)
@TestPropertySource(
        locations = "classpath:application-test.properties")
@Transactional
public class MembershipsDaoImplTest {
    @Autowired
    private GroupRepository dao;
    @Autowired
    private MembershipsDaoImpl membershipsDao;

    @Test
    public void getRole() {
        assertEquals(RoleInGroup.ADMIN, membershipsDao.getRole(1L, 2L));
    }

    @Test
    public void getRoleForNotMemberOfGroup() {
        assertNull(membershipsDao.getRole(1L, 5L));
    }

    @Test
    public void insertFollowerTest() {
        Memberships memberships = new Memberships();
        memberships.setGroupId(4L);
        memberships.setAccountId(3L);
        memberships.setRole(RoleInGroup.FOLLOWER);
        membershipsDao.insertFollower(memberships);
        assertEquals(RoleInGroup.FOLLOWER, membershipsDao.getRole(4L, 3L));
    }

    @Test(expected = Exception.class)
    public void insertFollowerExceptionTest() {
        Memberships memberships = new Memberships();
        memberships.setGroupId(null);
        memberships.setAccountId(null);
        memberships.setRole(RoleInGroup.FOLLOWER);
        membershipsDao.insertFollower(memberships);
    }

    @Test
    public void updateRoleTest() {
        Memberships memberships = new Memberships();
        memberships.setGroupId(1L);
        memberships.setAccountId(1L);
        memberships.setRole(RoleInGroup.ADMIN);
        membershipsDao.updateRole(memberships);
        assertEquals(RoleInGroup.ADMIN, membershipsDao.getRole(1L, 1L));
    }

    @Test
    public void deleteFollowerTest() {
        Memberships memberships = new Memberships();
        memberships.setGroupId(1L);
        memberships.setAccountId(4L);
        memberships.setRole(RoleInGroup.FOLLOWER);
        membershipsDao.insertFollower(memberships);
        membershipsDao.deleteFollower(new MembershipsKey(1L, 4L));
        Group group = dao.findById(1L).orElse(null);
        List<Account> friends = group != null ? group.getFollowers() : new ArrayList<>();
        Set<Long> followersId = new TreeSet<>();
        for (Account acc : friends) {
            followersId.add(acc.getId());
        }
        assertEquals(false, followersId.contains(4L));
    }
}
