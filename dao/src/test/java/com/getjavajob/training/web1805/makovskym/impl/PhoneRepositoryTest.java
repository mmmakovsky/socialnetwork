package com.getjavajob.training.web1805.makovskym.impl;

import com.getjavajob.training.web1805.makovskym.DaoApplication;
import com.getjavajob.training.web1805.makovskym.entities.Account;
import com.getjavajob.training.web1805.makovskym.entities.Phone;
import com.getjavajob.training.web1805.makovskym.enums.AccountRoles;
import com.getjavajob.training.web1805.makovskym.enums.PhoneType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;

import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = DaoApplication.class)
@TestPropertySource(
        locations = "classpath:application-test.properties")
@Transactional
public class PhoneRepositoryTest {
    @Autowired
    private PhoneRepository phoneRepository;

    @Test
    public void deleteByAccountTest() {
        Account newAccount = new Account("Korolev", null, "fffre", "1122334455", AccountRoles.USER, new Date(2018 - 5 - 11));
        newAccount.setId(3L);
        Phone phone = new Phone("+7(999)9090909", newAccount, PhoneType.HOME);
        phoneRepository.save(phone);
        phoneRepository.deleteByAccount(newAccount);
        assertNull(phoneRepository.findById(phone.getId()).orElse(null));
    }

}
